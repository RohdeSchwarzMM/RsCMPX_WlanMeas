Bw<BandwidthC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw40
	rc = driver.configure.wlanMeas.multiEval.limit.tsMask.htOfdm.bw.repcap_bandwidthC_get()
	driver.configure.wlanMeas.multiEval.limit.tsMask.htOfdm.bw.repcap_bandwidthC_set(repcap.BandwidthC.Bw5)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.HtOfdm.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.tsMask.htOfdm.bw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_TsMask_HtOfdm_Bw_AbsLimit.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_HtOfdm_Bw_Band.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_HtOfdm_Bw_Enable.rst