Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:CURRent
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:CURRent
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Segments.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: