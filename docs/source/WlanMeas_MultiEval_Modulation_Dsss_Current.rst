Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Dsss.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: