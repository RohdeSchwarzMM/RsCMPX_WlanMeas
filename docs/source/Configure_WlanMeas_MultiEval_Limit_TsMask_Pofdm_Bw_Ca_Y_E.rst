E
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:POFDm:BW<bandwidth>:CA:Y:E

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:POFDm:BW<bandwidth>:CA:Y:E



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.Pofdm.Bw.Ca.Y.E.ECls
	:members:
	:undoc-members:
	:noindex: