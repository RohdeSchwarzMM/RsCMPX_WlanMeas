Inphase
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:IQConst:INPHase
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:IQConst:INPHase

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:IQConst:INPHase
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:IQConst:INPHase



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.IqConstant.Inphase.InphaseCls
	:members:
	:undoc-members:
	:noindex: