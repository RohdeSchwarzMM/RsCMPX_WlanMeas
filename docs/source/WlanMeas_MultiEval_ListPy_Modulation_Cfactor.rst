Cfactor
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Cfactor.CfactorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.cfactor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Cfactor_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Cfactor_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Cfactor_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Cfactor_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Cfactor_StandardDev.rst