EvmEms
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.EvmEms.EvmEmsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dsss.evmEms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmEms_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmEms_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmEms_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmEms_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmEms_StandardDev.rst