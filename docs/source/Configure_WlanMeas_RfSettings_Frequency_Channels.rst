Channels<Channels>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.configure.wlanMeas.rfSettings.frequency.channels.repcap_channels_get()
	driver.configure.wlanMeas.rfSettings.frequency.channels.repcap_channels_set(repcap.Channels.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency:CHANnels<Ch>

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency:CHANnels<Ch>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.RfSettings.Frequency.Channels.ChannelsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.rfSettings.frequency.channels.clone()