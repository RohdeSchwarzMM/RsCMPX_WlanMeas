EhtOfdm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:EHTofdm:EVMall
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:EHTofdm:EVMPilot
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:EHTofdm:CFERror
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:EHTofdm:SCERror

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:EHTofdm:EVMall
	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:EHTofdm:EVMPilot
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:EHTofdm:CFERror
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:EHTofdm:SCERror



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.EhtOfdm.EhtOfdmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.modulation.ehtOfdm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_Modulation_EhtOfdm_CfoDistribution.rst
	Configure_WlanMeas_MultiEval_Limit_Modulation_EhtOfdm_IqOffset.rst