Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:AVERage
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:AVERage

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:AVERage
	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.FallingEdge.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: