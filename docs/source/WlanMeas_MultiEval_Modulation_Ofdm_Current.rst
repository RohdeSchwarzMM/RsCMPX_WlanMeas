Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Ofdm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: