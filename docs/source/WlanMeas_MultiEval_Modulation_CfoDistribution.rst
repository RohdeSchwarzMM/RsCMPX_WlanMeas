CfoDistribution
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CFDistrib
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CFDistrib
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:CFDistrib

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CFDistrib
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CFDistrib
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:CFDistrib



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.CfoDistribution.CfoDistributionCls
	:members:
	:undoc-members:
	:noindex: