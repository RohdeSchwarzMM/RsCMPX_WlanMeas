Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MAXimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MAXimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: