StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RUNit<ru>:RXANtenna<n>:SDEViation

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RUNit<ru>:RXANtenna<n>:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.Runit.RxAntenna.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: