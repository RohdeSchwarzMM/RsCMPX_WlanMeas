Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:DSSS:GIMBalance:MINimum

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:DSSS:GIMBalance:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.Gimbalance.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: