File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:TMODe:FILE:SAVE
	single: CONFigure:WLAN:MEASurement<instance>:TMODe:FILE:DATE

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:TMODe:FILE:SAVE
	CONFigure:WLAN:MEASurement<instance>:TMODe:FILE:DATE



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Tmode.File.FileCls
	:members:
	:undoc-members:
	:noindex: