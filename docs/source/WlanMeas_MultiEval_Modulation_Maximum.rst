Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: