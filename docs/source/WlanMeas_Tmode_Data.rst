Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CLEar:WLAN:MEASurement<instance>:TMODe:DATA

.. code-block:: python

	CLEar:WLAN:MEASurement<instance>:TMODe:DATA



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.Tmode.Data.DataCls
	:members:
	:undoc-members:
	:noindex: