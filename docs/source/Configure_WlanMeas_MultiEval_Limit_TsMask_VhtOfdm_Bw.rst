Bw<BandwidthE>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw8080
	rc = driver.configure.wlanMeas.multiEval.limit.tsMask.vhtOfdm.bw.repcap_bandwidthE_get()
	driver.configure.wlanMeas.multiEval.limit.tsMask.vhtOfdm.bw.repcap_bandwidthE_set(repcap.BandwidthE.Bw5)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.VhtOfdm.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.tsMask.vhtOfdm.bw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_TsMask_VhtOfdm_Bw_AbsLimit.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_VhtOfdm_Bw_Enable.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_VhtOfdm_Bw_Y.rst