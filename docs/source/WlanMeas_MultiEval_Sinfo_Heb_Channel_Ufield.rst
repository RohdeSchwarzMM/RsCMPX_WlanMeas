Ufield<UserIx>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr144
	rc = driver.wlanMeas.multiEval.sinfo.heb.channel.ufield.repcap_userIx_get()
	driver.wlanMeas.multiEval.sinfo.heb.channel.ufield.repcap_userIx_set(repcap.UserIx.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Heb.Channel.Ufield.UfieldCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.heb.channel.ufield.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_Coding.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_Crc.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_Dcm.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_Mcs.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_Nsts.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_Reserved.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_SpaConfig.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_StaId.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_Tail.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield_TxBeamforming.rst