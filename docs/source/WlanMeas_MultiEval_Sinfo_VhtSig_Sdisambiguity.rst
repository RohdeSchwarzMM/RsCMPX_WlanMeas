Sdisambiguity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:SDISambig

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:SDISambig



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.VhtSig.Sdisambiguity.SdisambiguityCls
	:members:
	:undoc-members:
	:noindex: