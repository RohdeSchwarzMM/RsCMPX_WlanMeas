StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:SDEViation
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:SDEViation
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Psts.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: