Stime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:STIMe

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:STIMe



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.Segment.Stime.StimeCls
	:members:
	:undoc-members:
	:noindex: