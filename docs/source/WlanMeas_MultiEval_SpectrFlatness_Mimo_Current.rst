Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:CURRent
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:CURRent
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: