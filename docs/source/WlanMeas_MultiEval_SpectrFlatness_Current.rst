Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:CURRent
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:CURRent
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: