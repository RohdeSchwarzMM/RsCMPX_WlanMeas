Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Mimo.Frequency.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: