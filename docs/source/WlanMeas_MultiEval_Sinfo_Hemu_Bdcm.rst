Bdcm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:BDCM

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:BDCM



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hemu.Bdcm.BdcmCls
	:members:
	:undoc-members:
	:noindex: