CfError
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:CFERror
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:CFERror

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:CFERror
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:CFERror



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.CfError.CfErrorCls
	:members:
	:undoc-members:
	:noindex: