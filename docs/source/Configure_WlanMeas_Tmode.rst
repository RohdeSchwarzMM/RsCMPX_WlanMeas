Tmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:TMODe:NOANtennas

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:TMODe:NOANtennas



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.tmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_Tmode_File.rst