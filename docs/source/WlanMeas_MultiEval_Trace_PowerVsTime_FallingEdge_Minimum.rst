Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.FallingEdge.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: