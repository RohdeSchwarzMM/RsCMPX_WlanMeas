Smi<Smi>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr4 .. Nr4
	rc = driver.route.wlanMeas.scenario.smi.repcap_smi_get()
	driver.route.wlanMeas.scenario.smi.repcap_smi_set(repcap.Smi.Nr4)



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:MEASurement<Instance>:SCENario:SMI<nr>

.. code-block:: python

	ROUTe:WLAN:MEASurement<Instance>:SCENario:SMI<nr>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Route.WlanMeas.Scenario.Smi.SmiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wlanMeas.scenario.smi.clone()