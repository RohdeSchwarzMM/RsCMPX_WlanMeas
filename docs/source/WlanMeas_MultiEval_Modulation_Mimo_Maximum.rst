Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:MAXimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:MAXimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Mimo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: