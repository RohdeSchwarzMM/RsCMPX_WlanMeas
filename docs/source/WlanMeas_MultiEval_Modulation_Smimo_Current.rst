Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:CURRent
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:CURRent
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Smimo.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: