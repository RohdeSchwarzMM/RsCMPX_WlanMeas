Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SCOunt:MODulation
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SCOunt:TSMask

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SCOunt:MODulation
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SCOunt:TSMask



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: