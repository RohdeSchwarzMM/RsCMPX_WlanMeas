Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: