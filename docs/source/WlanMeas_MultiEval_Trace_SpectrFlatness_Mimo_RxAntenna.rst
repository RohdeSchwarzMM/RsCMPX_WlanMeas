RxAntenna<RxAntenna>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.trace.spectrFlatness.mimo.rxAntenna.repcap_rxAntenna_get()
	driver.wlanMeas.multiEval.trace.spectrFlatness.mimo.rxAntenna.repcap_rxAntenna_set(repcap.RxAntenna.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Mimo.RxAntenna.RxAntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.spectrFlatness.mimo.rxAntenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_SpectrFlatness_Mimo_RxAntenna_Stream.rst