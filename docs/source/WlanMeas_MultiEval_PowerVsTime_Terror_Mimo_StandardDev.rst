StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:SDEViation
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:SDEViation
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:SDEViation
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:SDEViation
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.Mimo.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: