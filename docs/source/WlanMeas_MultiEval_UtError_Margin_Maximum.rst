Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Margin.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: