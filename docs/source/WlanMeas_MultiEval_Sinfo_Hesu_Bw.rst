Bw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:BW

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:BW



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hesu.Bw.BwCls
	:members:
	:undoc-members:
	:noindex: