Pofdm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:EVM
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:EVMPilot
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:IQOFfset
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:CFERror
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:SCERror

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:EVM
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:EVMPilot
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:IQOFfset
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:CFERror
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:POFDm:SCERror



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.Pofdm.PofdmCls
	:members:
	:undoc-members:
	:noindex: