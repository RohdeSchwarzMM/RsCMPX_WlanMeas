E
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:POFDm:BW<bandwidth>:CB:Y:E

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:POFDm:BW<bandwidth>:CB:Y:E



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.Pofdm.Bw.Cb.Y.E.ECls
	:members:
	:undoc-members:
	:noindex: