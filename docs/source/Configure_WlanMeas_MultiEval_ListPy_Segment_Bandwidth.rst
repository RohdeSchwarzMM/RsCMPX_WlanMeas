Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:BWIDth

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:BWIDth



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.Segment.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: