RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst32
	# All values (32x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16
	Inst17 | Inst18 | Inst19 | Inst20 | Inst21 | Inst22 | Inst23 | Inst24
	Inst25 | Inst26 | Inst27 | Inst28 | Inst29 | Inst30 | Inst31 | Inst32

Antenna
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Antenna.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Band.Nr2
	# Values (2x):
	Nr2 | Nr5

BandwidthA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandwidthA.Bw10
	# Values (2x):
	Bw10 | Bw20

BandwidthB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandwidthB.Bw5
	# Values (3x):
	Bw5 | Bw10 | Bw20

BandwidthC
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandwidthC.Bw5
	# Values (4x):
	Bw5 | Bw10 | Bw20 | Bw40

BandwidthD
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandwidthD.Bw20
	# Range:
	Bw20 .. Bw8080
	# All values (5x):
	Bw20 | Bw40 | Bw80 | Bw160 | Bw8080

BandwidthE
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandwidthE.Bw5
	# Range:
	Bw5 .. Bw8080
	# All values (7x):
	Bw5 | Bw10 | Bw20 | Bw40 | Bw80 | Bw160 | Bw8080

BandwidthF
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandwidthF.Bw20
	# Range:
	Bw20 .. Bw320
	# All values (5x):
	Bw20 | Bw40 | Bw80 | Bw160 | Bw320

BandwidthG
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandwidthG.Bw5
	# Range:
	Bw5 .. Bw320
	# All values (7x):
	Bw5 | Bw10 | Bw20 | Bw40 | Bw80 | Bw160 | Bw320

Channel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Channel.Nr1
	# Values (2x):
	Nr1 | Nr2

Channels
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Channels.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

Connector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Connector.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

Mimo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Mimo.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

Reserved
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Reserved.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

ResourceUnit
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ResourceUnit.Nr1
	# Range:
	Nr1 .. Nr144
	# All values (144x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64
	Nr65 | Nr66 | Nr67 | Nr68 | Nr69 | Nr70 | Nr71 | Nr72
	Nr73 | Nr74 | Nr75 | Nr76 | Nr77 | Nr78 | Nr79 | Nr80
	Nr81 | Nr82 | Nr83 | Nr84 | Nr85 | Nr86 | Nr87 | Nr88
	Nr89 | Nr90 | Nr91 | Nr92 | Nr93 | Nr94 | Nr95 | Nr96
	Nr97 | Nr98 | Nr99 | Nr100 | Nr101 | Nr102 | Nr103 | Nr104
	Nr105 | Nr106 | Nr107 | Nr108 | Nr109 | Nr110 | Nr111 | Nr112
	Nr113 | Nr114 | Nr115 | Nr116 | Nr117 | Nr118 | Nr119 | Nr120
	Nr121 | Nr122 | Nr123 | Nr124 | Nr125 | Nr126 | Nr127 | Nr128
	Nr129 | Nr130 | Nr131 | Nr132 | Nr133 | Nr134 | Nr135 | Nr136
	Nr137 | Nr138 | Nr139 | Nr140 | Nr141 | Nr142 | Nr143 | Nr144

RxAntenna
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RxAntenna.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

Segment
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Segment.Nr1
	# Values (2x):
	Nr1 | Nr2

SegmentB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SegmentB.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

Smi
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Smi.Nr4
	# Values (1x):
	Nr4

SMimoPath
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SMimoPath.Count2
	# Values (3x):
	Count2 | Count4 | Count8

Spatial
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Spatial.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

Stream
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Stream.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

TrueMimoPath
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.TrueMimoPath.Count1
	# Values (4x):
	Count1 | Count2 | Count3 | Count4

User
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.User.Nr1
	# Range:
	Nr1 .. Nr144
	# All values (144x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64
	Nr65 | Nr66 | Nr67 | Nr68 | Nr69 | Nr70 | Nr71 | Nr72
	Nr73 | Nr74 | Nr75 | Nr76 | Nr77 | Nr78 | Nr79 | Nr80
	Nr81 | Nr82 | Nr83 | Nr84 | Nr85 | Nr86 | Nr87 | Nr88
	Nr89 | Nr90 | Nr91 | Nr92 | Nr93 | Nr94 | Nr95 | Nr96
	Nr97 | Nr98 | Nr99 | Nr100 | Nr101 | Nr102 | Nr103 | Nr104
	Nr105 | Nr106 | Nr107 | Nr108 | Nr109 | Nr110 | Nr111 | Nr112
	Nr113 | Nr114 | Nr115 | Nr116 | Nr117 | Nr118 | Nr119 | Nr120
	Nr121 | Nr122 | Nr123 | Nr124 | Nr125 | Nr126 | Nr127 | Nr128
	Nr129 | Nr130 | Nr131 | Nr132 | Nr133 | Nr134 | Nr135 | Nr136
	Nr137 | Nr138 | Nr139 | Nr140 | Nr141 | Nr142 | Nr143 | Nr144

UserIx
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UserIx.Nr1
	# Range:
	Nr1 .. Nr144
	# All values (144x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64
	Nr65 | Nr66 | Nr67 | Nr68 | Nr69 | Nr70 | Nr71 | Nr72
	Nr73 | Nr74 | Nr75 | Nr76 | Nr77 | Nr78 | Nr79 | Nr80
	Nr81 | Nr82 | Nr83 | Nr84 | Nr85 | Nr86 | Nr87 | Nr88
	Nr89 | Nr90 | Nr91 | Nr92 | Nr93 | Nr94 | Nr95 | Nr96
	Nr97 | Nr98 | Nr99 | Nr100 | Nr101 | Nr102 | Nr103 | Nr104
	Nr105 | Nr106 | Nr107 | Nr108 | Nr109 | Nr110 | Nr111 | Nr112
	Nr113 | Nr114 | Nr115 | Nr116 | Nr117 | Nr118 | Nr119 | Nr120
	Nr121 | Nr122 | Nr123 | Nr124 | Nr125 | Nr126 | Nr127 | Nr128
	Nr129 | Nr130 | Nr131 | Nr132 | Nr133 | Nr134 | Nr135 | Nr136
	Nr137 | Nr138 | Nr139 | Nr140 | Nr141 | Nr142 | Nr143 | Nr144

UtError
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UtError.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

