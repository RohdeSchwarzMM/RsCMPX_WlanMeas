Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Segments.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: