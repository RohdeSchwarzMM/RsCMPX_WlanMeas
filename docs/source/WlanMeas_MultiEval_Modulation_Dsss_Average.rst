Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Dsss.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: