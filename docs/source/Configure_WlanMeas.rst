WlanMeas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MODE

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MODE



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.WlanMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_Isignal.rst
	Configure_WlanMeas_Mimo.rst
	Configure_WlanMeas_MultiEval.rst
	Configure_WlanMeas_RfSettings.rst
	Configure_WlanMeas_Smimo.rst
	Configure_WlanMeas_Tmode.rst