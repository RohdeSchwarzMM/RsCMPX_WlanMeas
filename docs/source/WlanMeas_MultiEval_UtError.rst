UtError<UtError>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.utError.repcap_utError_get()
	driver.wlanMeas.multiEval.utError.repcap_utError_set(repcap.UtError.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.UtErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.utError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_UtError_Average.rst
	WlanMeas_MultiEval_UtError_Current.rst
	WlanMeas_MultiEval_UtError_Limit.rst
	WlanMeas_MultiEval_UtError_Margin.rst
	WlanMeas_MultiEval_UtError_Maximum.rst
	WlanMeas_MultiEval_UtError_Minimum.rst