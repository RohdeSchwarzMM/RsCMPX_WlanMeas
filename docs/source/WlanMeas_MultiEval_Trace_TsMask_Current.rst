Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: