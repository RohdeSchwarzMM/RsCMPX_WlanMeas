Stream<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.trace.spectrFlatness.mimo.rxAntenna.stream.repcap_stream_get()
	driver.wlanMeas.multiEval.trace.spectrFlatness.mimo.rxAntenna.stream.repcap_stream_set(repcap.Stream.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Mimo.RxAntenna.Stream.StreamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.spectrFlatness.mimo.rxAntenna.stream.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_SpectrFlatness_Mimo_RxAntenna_Stream_Acarrier.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Mimo_RxAntenna_Stream_Average.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Mimo_RxAntenna_Stream_Current.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Mimo_RxAntenna_Stream_Maximum.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Mimo_RxAntenna_Stream_Minimum.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Mimo_RxAntenna_Stream_Segment.rst