Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:MAXimum
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:MAXimum

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:MAXimum
	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.FallingEdge.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: