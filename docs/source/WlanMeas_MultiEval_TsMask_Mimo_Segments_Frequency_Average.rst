Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Mimo.Segments.Frequency.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: