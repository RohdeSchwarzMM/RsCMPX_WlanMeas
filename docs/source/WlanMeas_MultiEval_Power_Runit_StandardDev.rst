StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RUNit<ru>:SDEViation

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RUNit<ru>:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.Runit.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: