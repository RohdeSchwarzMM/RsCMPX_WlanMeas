TxOp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:TXOP

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:TXOP



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.VhtSig.TxOp.TxOpCls
	:members:
	:undoc-members:
	:noindex: