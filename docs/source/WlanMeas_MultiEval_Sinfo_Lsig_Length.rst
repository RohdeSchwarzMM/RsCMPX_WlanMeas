Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:LENGth

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:LENGth



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Lsig.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: