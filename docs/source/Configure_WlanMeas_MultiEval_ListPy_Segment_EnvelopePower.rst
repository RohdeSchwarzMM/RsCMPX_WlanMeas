EnvelopePower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:ENPower

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:ENPower



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.Segment.EnvelopePower.EnvelopePowerCls
	:members:
	:undoc-members:
	:noindex: