Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:MAXimum
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:MAXimum

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:MAXimum
	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.RisingEdge.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: