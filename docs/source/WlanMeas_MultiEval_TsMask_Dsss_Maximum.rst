Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Dsss.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: