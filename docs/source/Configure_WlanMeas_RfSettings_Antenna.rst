Antenna<Antenna>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.configure.wlanMeas.rfSettings.antenna.repcap_antenna_get()
	driver.configure.wlanMeas.rfSettings.antenna.repcap_antenna_set(repcap.Antenna.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:ANTenna<n>

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:RFSettings:ANTenna<n>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.RfSettings.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.rfSettings.antenna.clone()