LrStart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:LRSTart

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:RFSettings:LRSTart



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.RfSettings.LrStart.LrStartCls
	:members:
	:undoc-members:
	:noindex: