Y
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:DSSS:Y:AB
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:DSSS:Y:CD

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:DSSS:Y:AB
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:DSSS:Y:CD



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.Dsss.Y.YCls
	:members:
	:undoc-members:
	:noindex: