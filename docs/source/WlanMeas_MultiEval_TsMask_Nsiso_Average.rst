Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Nsiso.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: