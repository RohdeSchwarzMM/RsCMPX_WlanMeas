Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:PVTime
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:SFLatness
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult[:ALL]
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:EVM
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:EVMCarrier
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:IQConst
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:UTERror
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:EVMSymbol
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:TSMask
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:MSCalar

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:PVTime
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:SFLatness
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult[:ALL]
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:EVM
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:EVMCarrier
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:IQConst
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:UTERror
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:EVMSymbol
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:TSMask
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:RESult:MSCalar



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: