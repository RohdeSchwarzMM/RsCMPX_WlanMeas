StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:SDEViation
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:SDEViation
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:SDEViation
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:SDEViation
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: