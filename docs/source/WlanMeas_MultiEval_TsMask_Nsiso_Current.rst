Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Nsiso.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: