Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.X.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: