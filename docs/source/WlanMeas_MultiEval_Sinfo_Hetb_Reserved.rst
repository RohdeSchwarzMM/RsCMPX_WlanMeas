Reserved<Reserved>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.wlanMeas.multiEval.sinfo.hetb.reserved.repcap_reserved_get()
	driver.wlanMeas.multiEval.sinfo.hetb.reserved.repcap_reserved_set(repcap.Reserved.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:REServed<index>

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:REServed<index>



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hetb.Reserved.ReservedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.hetb.reserved.clone()