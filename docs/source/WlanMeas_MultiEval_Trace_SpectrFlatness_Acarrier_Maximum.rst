Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Acarrier.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: