SpectrFlatness
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.SpectrFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.spectrFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_SpectrFlatness_Average.rst
	WlanMeas_MultiEval_SpectrFlatness_Current.rst
	WlanMeas_MultiEval_SpectrFlatness_Maximum.rst
	WlanMeas_MultiEval_SpectrFlatness_Mimo.rst
	WlanMeas_MultiEval_SpectrFlatness_Minimum.rst
	WlanMeas_MultiEval_SpectrFlatness_X.rst