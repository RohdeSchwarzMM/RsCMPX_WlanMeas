Hesu
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hesu.HesuCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.hesu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Hesu_BeamChange.rst
	WlanMeas_MultiEval_Sinfo_Hesu_BssColor.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Bw.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Coding.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Crc.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Dcm.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Doppler.rst
	WlanMeas_MultiEval_Sinfo_Hesu_FormatPy.rst
	WlanMeas_MultiEval_Sinfo_Hesu_GiltfSize.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Ldpc.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Mcs.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Nsts.rst
	WlanMeas_MultiEval_Sinfo_Hesu_PeDisambiguity.rst
	WlanMeas_MultiEval_Sinfo_Hesu_PfecPadding.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Reserved.rst
	WlanMeas_MultiEval_Sinfo_Hesu_SpatialReuse.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Stbc.rst
	WlanMeas_MultiEval_Sinfo_Hesu_Tail.rst
	WlanMeas_MultiEval_Sinfo_Hesu_TxBf.rst
	WlanMeas_MultiEval_Sinfo_Hesu_TxOp.rst
	WlanMeas_MultiEval_Sinfo_Hesu_UlDl.rst