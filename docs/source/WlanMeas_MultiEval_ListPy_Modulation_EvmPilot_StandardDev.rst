StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:EVMPilot:SDEViation

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:EVMPilot:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.EvmPilot.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: