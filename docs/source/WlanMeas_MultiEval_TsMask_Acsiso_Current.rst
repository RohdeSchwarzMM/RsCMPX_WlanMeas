Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Acsiso.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: