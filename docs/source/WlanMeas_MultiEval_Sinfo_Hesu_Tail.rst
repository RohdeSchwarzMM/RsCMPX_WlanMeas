Tail
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:TAIL

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:TAIL



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hesu.Tail.TailCls
	:members:
	:undoc-members:
	:noindex: