EvmPeak
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.EvmPeak.EvmPeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dsss.evmPeak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmPeak_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmPeak_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmPeak_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmPeak_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_EvmPeak_StandardDev.rst