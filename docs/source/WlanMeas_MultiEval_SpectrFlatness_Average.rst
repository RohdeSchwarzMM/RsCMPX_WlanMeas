Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:AVERage
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:AVERage
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: