Hetb
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hetb.HetbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.hetb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Hetb_BssColor.rst
	WlanMeas_MultiEval_Sinfo_Hetb_Bw.rst
	WlanMeas_MultiEval_Sinfo_Hetb_Crc.rst
	WlanMeas_MultiEval_Sinfo_Hetb_FormatPy.rst
	WlanMeas_MultiEval_Sinfo_Hetb_Reserved.rst
	WlanMeas_MultiEval_Sinfo_Hetb_SpatialReuse.rst
	WlanMeas_MultiEval_Sinfo_Hetb_Tail.rst
	WlanMeas_MultiEval_Sinfo_Hetb_TxOp.rst