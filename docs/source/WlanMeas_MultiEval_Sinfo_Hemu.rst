Hemu
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hemu.HemuCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.hemu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Hemu_Bdcm.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Bmcs.rst
	WlanMeas_MultiEval_Sinfo_Hemu_BssColor.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Bw.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Crc.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Doppler.rst
	WlanMeas_MultiEval_Sinfo_Hemu_GiltfSize.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Ldpc.rst
	WlanMeas_MultiEval_Sinfo_Hemu_NltfSymbols.rst
	WlanMeas_MultiEval_Sinfo_Hemu_NsbSymbols.rst
	WlanMeas_MultiEval_Sinfo_Hemu_PeDisambiguity.rst
	WlanMeas_MultiEval_Sinfo_Hemu_PfecPadding.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Reserved.rst
	WlanMeas_MultiEval_Sinfo_Hemu_SbCompress.rst
	WlanMeas_MultiEval_Sinfo_Hemu_SpatialReuse.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Stbc.rst
	WlanMeas_MultiEval_Sinfo_Hemu_Tail.rst
	WlanMeas_MultiEval_Sinfo_Hemu_TxOp.rst
	WlanMeas_MultiEval_Sinfo_Hemu_UlDl.rst