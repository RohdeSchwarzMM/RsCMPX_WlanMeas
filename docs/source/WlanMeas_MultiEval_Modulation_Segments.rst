Segments
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Segments.SegmentsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.segments.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_Segments_Average.rst
	WlanMeas_MultiEval_Modulation_Segments_Current.rst
	WlanMeas_MultiEval_Modulation_Segments_Maximum.rst
	WlanMeas_MultiEval_Modulation_Segments_Minimum.rst
	WlanMeas_MultiEval_Modulation_Segments_StandardDev.rst