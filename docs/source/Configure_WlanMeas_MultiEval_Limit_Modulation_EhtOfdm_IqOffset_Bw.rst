Bw<BandwidthG>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw320
	rc = driver.configure.wlanMeas.multiEval.limit.modulation.ehtOfdm.iqOffset.bw.repcap_bandwidthG_get()
	driver.configure.wlanMeas.multiEval.limit.modulation.ehtOfdm.iqOffset.bw.repcap_bandwidthG_set(repcap.BandwidthG.Bw5)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:EHTofdm:IQOFfset:BW<BW>

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:EHTofdm:IQOFfset:BW<BW>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.EhtOfdm.IqOffset.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.modulation.ehtOfdm.iqOffset.bw.clone()