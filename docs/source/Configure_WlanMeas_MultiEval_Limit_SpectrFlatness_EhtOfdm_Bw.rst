Bw<BandwidthF>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw20 .. Bw320
	rc = driver.configure.wlanMeas.multiEval.limit.spectrFlatness.ehtOfdm.bw.repcap_bandwidthF_get()
	driver.configure.wlanMeas.multiEval.limit.spectrFlatness.ehtOfdm.bw.repcap_bandwidthF_set(repcap.BandwidthF.Bw20)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.SpectrFlatness.EhtOfdm.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.spectrFlatness.ehtOfdm.bw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_EhtOfdm_Bw_Enable.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_EhtOfdm_Bw_Lower.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_EhtOfdm_Bw_Upper.rst