Ppower
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Ppower.PpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.ppower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Ppower_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Ppower_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Ppower_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Ppower_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Ppower_StandardDev.rst