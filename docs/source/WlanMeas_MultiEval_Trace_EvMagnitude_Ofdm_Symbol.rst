Symbol
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Ofdm.Symbol.SymbolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.evMagnitude.ofdm.symbol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_EvMagnitude_Ofdm_Symbol_Average.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Ofdm_Symbol_Current.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Ofdm_Symbol_Maximum.rst