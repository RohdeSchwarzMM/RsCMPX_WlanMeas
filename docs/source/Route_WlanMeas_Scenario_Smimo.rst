Smimo<SMimoPath>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Count2 .. Count8
	rc = driver.route.wlanMeas.scenario.smimo.repcap_sMimoPath_get()
	driver.route.wlanMeas.scenario.smimo.repcap_sMimoPath_set(repcap.SMimoPath.Count2)



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:MEASurement<Instance>:SCENario:SMIMo<PathCount>

.. code-block:: python

	ROUTe:WLAN:MEASurement<Instance>:SCENario:SMIMo<PathCount>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Route.WlanMeas.Scenario.Smimo.SmimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wlanMeas.scenario.smimo.clone()