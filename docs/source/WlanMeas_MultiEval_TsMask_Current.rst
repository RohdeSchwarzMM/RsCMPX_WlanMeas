Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: