EvMagnitude
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_EvMagnitude_Average.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_Current.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_Maximum.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_StandardDev.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User.rst