TeDistribution
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:MIMO<n>:TEDistrib
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:MIMO<n>:TEDistrib
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:MIMO<n>:TEDistrib

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:MIMO<n>:TEDistrib
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:MIMO<n>:TEDistrib
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:MIMO<n>:TEDistrib



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Mimo.TeDistribution.TeDistributionCls
	:members:
	:undoc-members:
	:noindex: