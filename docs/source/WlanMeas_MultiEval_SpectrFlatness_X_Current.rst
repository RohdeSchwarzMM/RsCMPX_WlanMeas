Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.X.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: