Bw<BandwidthF>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw20 .. Bw320
	rc = driver.configure.wlanMeas.multiEval.limit.tsMask.ehtOfdm.bw.repcap_bandwidthF_get()
	driver.configure.wlanMeas.multiEval.limit.tsMask.ehtOfdm.bw.repcap_bandwidthF_set(repcap.BandwidthF.Bw20)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.EhtOfdm.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.tsMask.ehtOfdm.bw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_TsMask_EhtOfdm_Bw_AbsLimit.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_EhtOfdm_Bw_Enable.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_EhtOfdm_Bw_Y.rst