Band<Band>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr2 .. Nr5
	rc = driver.configure.wlanMeas.multiEval.limit.tsMask.htOfdm.bw.band.repcap_band_get()
	driver.configure.wlanMeas.multiEval.limit.tsMask.htOfdm.bw.band.repcap_band_set(repcap.Band.Nr2)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.HtOfdm.Bw.Band.BandCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.tsMask.htOfdm.bw.band.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_TsMask_HtOfdm_Bw_Band_Y.rst