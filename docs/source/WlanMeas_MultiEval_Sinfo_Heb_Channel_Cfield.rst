Cfield
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Heb.Channel.Cfield.CfieldCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.heb.channel.cfield.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Heb_Channel_Cfield_Crc.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Cfield_Cru.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Cfield_RuAllocation.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Cfield_Tail.rst