Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Symbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: