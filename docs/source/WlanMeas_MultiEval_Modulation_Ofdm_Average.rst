Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Ofdm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: