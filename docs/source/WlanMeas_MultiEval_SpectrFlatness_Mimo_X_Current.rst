Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.X.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: