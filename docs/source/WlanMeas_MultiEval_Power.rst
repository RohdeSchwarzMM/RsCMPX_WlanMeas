Power
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Power_Runit.rst
	WlanMeas_MultiEval_Power_RxAntenna.rst