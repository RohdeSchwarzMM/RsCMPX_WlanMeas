Bw<BandwidthD>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw20 .. Bw8080
	rc = driver.configure.wlanMeas.multiEval.limit.spectrFlatness.heOfdm.bw.repcap_bandwidthD_get()
	driver.configure.wlanMeas.multiEval.limit.spectrFlatness.heOfdm.bw.repcap_bandwidthD_set(repcap.BandwidthD.Bw20)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.SpectrFlatness.HeOfdm.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.spectrFlatness.heOfdm.bw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_HeOfdm_Bw_Enable.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_HeOfdm_Bw_Lower.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_HeOfdm_Bw_Upper.rst