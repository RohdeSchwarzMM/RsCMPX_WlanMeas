MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:CFOestimate
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:EMEThod
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:SMODe

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:CFOestimate
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:EMEThod
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:SMODe



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Compensation.rst
	Configure_WlanMeas_MultiEval_Demod.rst
	Configure_WlanMeas_MultiEval_Limit.rst
	Configure_WlanMeas_MultiEval_ListPy.rst
	Configure_WlanMeas_MultiEval_PowerVsTime.rst
	Configure_WlanMeas_MultiEval_Result.rst
	Configure_WlanMeas_MultiEval_Scount.rst
	Configure_WlanMeas_MultiEval_SpectrFlatness.rst
	Configure_WlanMeas_MultiEval_TsMask.rst