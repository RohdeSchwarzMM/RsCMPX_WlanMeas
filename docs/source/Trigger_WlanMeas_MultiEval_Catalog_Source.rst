Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WLAN:MEASurement<Instance>:MEValuation:CATalog:SOURce

.. code-block:: python

	TRIGger:WLAN:MEASurement<Instance>:MEValuation:CATalog:SOURce



.. autoclass:: RsCMPX_WlanMeas.Implementations.Trigger.WlanMeas.MultiEval.Catalog.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: