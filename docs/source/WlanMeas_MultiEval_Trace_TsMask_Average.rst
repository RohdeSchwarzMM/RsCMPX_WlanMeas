Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: