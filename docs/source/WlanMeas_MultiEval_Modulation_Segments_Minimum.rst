Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MINimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MINimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MINimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MINimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Segments.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: