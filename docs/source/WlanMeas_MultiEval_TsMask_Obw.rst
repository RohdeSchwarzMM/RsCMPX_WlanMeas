Obw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.tsMask.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_TsMask_Obw_Mimo.rst
	WlanMeas_MultiEval_TsMask_Obw_Segments.rst