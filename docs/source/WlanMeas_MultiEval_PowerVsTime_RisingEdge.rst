RisingEdge
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.RisingEdge.RisingEdgeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.powerVsTime.risingEdge.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_PowerVsTime_RisingEdge_Average.rst
	WlanMeas_MultiEval_PowerVsTime_RisingEdge_Current.rst
	WlanMeas_MultiEval_PowerVsTime_RisingEdge_Maximum.rst