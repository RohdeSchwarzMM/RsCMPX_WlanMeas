Lofdm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:EVM
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:EVMPilot
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:IQOFfset
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:CFERror
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:SCERror

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:EVM
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:EVMPilot
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:IQOFfset
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:CFERror
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:LOFDm:SCERror



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.Lofdm.LofdmCls
	:members:
	:undoc-members:
	:noindex: