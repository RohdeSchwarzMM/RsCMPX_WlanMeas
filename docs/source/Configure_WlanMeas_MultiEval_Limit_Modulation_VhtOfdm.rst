VhtOfdm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:VHTofdm:EVMall
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:VHTofdm:EVMPilot
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:VHTofdm:CFERror
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:VHTofdm:SCERror

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:VHTofdm:EVMall
	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:VHTofdm:EVMPilot
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:VHTofdm:CFERror
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:VHTofdm:SCERror



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.VhtOfdm.VhtOfdmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.modulation.vhtOfdm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_Modulation_VhtOfdm_IqOffset.rst