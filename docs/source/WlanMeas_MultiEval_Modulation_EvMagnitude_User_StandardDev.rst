StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:EVMagnitude:USER<user>:SDEViation

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:EVMagnitude:USER<user>:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.EvMagnitude.User.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: