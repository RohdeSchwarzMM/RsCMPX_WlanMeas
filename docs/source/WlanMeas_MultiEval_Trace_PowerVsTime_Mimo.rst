Mimo<Mimo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.trace.powerVsTime.mimo.repcap_mimo_get()
	driver.wlanMeas.multiEval.trace.powerVsTime.mimo.repcap_mimo_set(repcap.Mimo.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.powerVsTime.mimo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_PowerVsTime_Mimo_Average.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_Mimo_Current.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_Mimo_Maximum.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_Mimo_Minimum.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_Mimo_Segment.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_Mimo_Time.rst