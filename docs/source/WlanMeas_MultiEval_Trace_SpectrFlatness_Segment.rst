Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.wlanMeas.multiEval.trace.spectrFlatness.segment.repcap_segment_get()
	driver.wlanMeas.multiEval.trace.spectrFlatness.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.spectrFlatness.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_SpectrFlatness_Segment_Average.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Segment_Current.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Segment_Maximum.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Segment_Minimum.rst