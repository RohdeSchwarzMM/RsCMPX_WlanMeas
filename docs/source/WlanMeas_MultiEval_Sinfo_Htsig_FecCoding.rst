FecCoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:FECCoding

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:FECCoding



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Htsig.FecCoding.FecCodingCls
	:members:
	:undoc-members:
	:noindex: