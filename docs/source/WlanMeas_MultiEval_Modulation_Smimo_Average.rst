Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:AVERage
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:AVERage
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Smimo.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: