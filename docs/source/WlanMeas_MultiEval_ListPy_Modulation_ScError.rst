ScError
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.ScError.ScErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.scError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_ScError_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_ScError_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_ScError_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_ScError_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_ScError_StandardDev.rst