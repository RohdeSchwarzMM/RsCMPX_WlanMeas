BssColor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:BSSColor

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:BSSColor



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hetb.BssColor.BssColorCls
	:members:
	:undoc-members:
	:noindex: