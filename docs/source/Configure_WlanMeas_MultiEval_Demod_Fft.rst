Fft
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:DEMod:FFT:OFFSet

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:DEMod:FFT:OFFSet



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Demod.Fft.FftCls
	:members:
	:undoc-members:
	:noindex: