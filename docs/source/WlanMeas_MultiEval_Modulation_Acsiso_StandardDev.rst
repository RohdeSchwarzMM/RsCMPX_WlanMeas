StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:SDEViation
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:SDEViation
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:SDEViation
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:SDEViation
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Acsiso.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: