Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:AVERage
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:AVERage
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Segments.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: