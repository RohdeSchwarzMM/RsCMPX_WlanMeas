Psts
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Psts.PstsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.cmimo.psts.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_Cmimo_Psts_Average.rst
	WlanMeas_MultiEval_Modulation_Cmimo_Psts_Current.rst
	WlanMeas_MultiEval_Modulation_Cmimo_Psts_Maximum.rst
	WlanMeas_MultiEval_Modulation_Cmimo_Psts_Minimum.rst
	WlanMeas_MultiEval_Modulation_Cmimo_Psts_StandardDev.rst