Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MINimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MINimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MINimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MINimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: