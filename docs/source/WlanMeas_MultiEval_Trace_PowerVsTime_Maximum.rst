Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: