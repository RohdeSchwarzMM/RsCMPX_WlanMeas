Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:MIMO<n>:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:MIMO<n>:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:MIMO<n>:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:MIMO<n>:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Symbol.Mimo.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: