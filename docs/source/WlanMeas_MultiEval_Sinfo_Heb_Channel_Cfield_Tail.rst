Tail
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEB:CHANnel<ch_index>:CFIeld:TAIL

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEB:CHANnel<ch_index>:CFIeld:TAIL



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Heb.Channel.Cfield.Tail.TailCls
	:members:
	:undoc-members:
	:noindex: