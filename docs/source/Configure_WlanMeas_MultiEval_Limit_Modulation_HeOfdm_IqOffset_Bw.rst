Bw<BandwidthE>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw8080
	rc = driver.configure.wlanMeas.multiEval.limit.modulation.heOfdm.iqOffset.bw.repcap_bandwidthE_get()
	driver.configure.wlanMeas.multiEval.limit.modulation.heOfdm.iqOffset.bw.repcap_bandwidthE_set(repcap.BandwidthE.Bw5)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:HEOFdm:IQOFfset:BW<BW>

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:HEOFdm:IQOFfset:BW<BW>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.HeOfdm.IqOffset.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.modulation.heOfdm.iqOffset.bw.clone()