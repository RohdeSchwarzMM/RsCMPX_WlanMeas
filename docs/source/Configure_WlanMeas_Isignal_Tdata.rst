Tdata
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:ISIGnal:TDATa

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:ISIGnal:TDATa



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Isignal.Tdata.TdataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.isignal.tdata.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_Isignal_Tdata_File.rst