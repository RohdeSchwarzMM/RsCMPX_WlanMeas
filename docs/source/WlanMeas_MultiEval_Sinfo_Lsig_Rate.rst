Rate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:RATE

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:RATE



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Lsig.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: