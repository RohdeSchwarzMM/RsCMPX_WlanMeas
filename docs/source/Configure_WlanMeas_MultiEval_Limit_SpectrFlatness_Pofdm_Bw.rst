Bw<BandwidthB>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw20
	rc = driver.configure.wlanMeas.multiEval.limit.spectrFlatness.pofdm.bw.repcap_bandwidthB_get()
	driver.configure.wlanMeas.multiEval.limit.spectrFlatness.pofdm.bw.repcap_bandwidthB_set(repcap.BandwidthB.Bw5)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.SpectrFlatness.Pofdm.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.spectrFlatness.pofdm.bw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_Pofdm_Bw_Enable.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_Pofdm_Bw_Lower.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_Pofdm_Bw_Upper.rst