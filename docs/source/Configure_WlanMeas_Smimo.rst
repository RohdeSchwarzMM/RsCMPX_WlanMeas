Smimo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:SMIMo:CTUPle

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:SMIMo:CTUPle



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Smimo.SmimoCls
	:members:
	:undoc-members:
	:noindex: