Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MAXimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MAXimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Segments.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: