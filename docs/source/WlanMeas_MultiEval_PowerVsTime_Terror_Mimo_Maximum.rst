Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.Mimo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: