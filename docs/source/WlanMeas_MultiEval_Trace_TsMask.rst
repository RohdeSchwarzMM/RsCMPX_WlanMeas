TsMask
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.TsMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.tsMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_TsMask_Average.rst
	WlanMeas_MultiEval_Trace_TsMask_Current.rst
	WlanMeas_MultiEval_Trace_TsMask_Frequency.rst
	WlanMeas_MultiEval_Trace_TsMask_Mask.rst
	WlanMeas_MultiEval_Trace_TsMask_Maximum.rst
	WlanMeas_MultiEval_Trace_TsMask_Mimo.rst
	WlanMeas_MultiEval_Trace_TsMask_Minimum.rst
	WlanMeas_MultiEval_Trace_TsMask_Segment.rst