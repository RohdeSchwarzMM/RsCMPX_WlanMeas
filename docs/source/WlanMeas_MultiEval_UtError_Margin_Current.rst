Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Margin.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: