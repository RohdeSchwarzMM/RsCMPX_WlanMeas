Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Mimo.Segments.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: