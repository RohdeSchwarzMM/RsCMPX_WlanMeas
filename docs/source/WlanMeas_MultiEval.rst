MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:WLAN:MEASurement<Instance>:MEValuation
	single: ABORt:WLAN:MEASurement<Instance>:MEValuation
	single: INITiate:WLAN:MEASurement<Instance>:MEValuation

.. code-block:: python

	STOP:WLAN:MEASurement<Instance>:MEValuation
	ABORt:WLAN:MEASurement<Instance>:MEValuation
	INITiate:WLAN:MEASurement<Instance>:MEValuation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy.rst
	WlanMeas_MultiEval_Modulation.rst
	WlanMeas_MultiEval_Ofdma.rst
	WlanMeas_MultiEval_Power.rst
	WlanMeas_MultiEval_PowerVsTime.rst
	WlanMeas_MultiEval_Sinfo.rst
	WlanMeas_MultiEval_SpectrFlatness.rst
	WlanMeas_MultiEval_State.rst
	WlanMeas_MultiEval_Trace.rst
	WlanMeas_MultiEval_TsMask.rst
	WlanMeas_MultiEval_UtError.rst