Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RXANtenna<n>:CURRent

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RXANtenna<n>:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.RxAntenna.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: