Dsss
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:EVMRms
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:EVMPeak
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:IQOFfset
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:CFERror
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:CCERror

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:EVMRms
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:EVMPeak
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:IQOFfset
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:CFERror
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:DSSS:CCERror



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.Dsss.DsssCls
	:members:
	:undoc-members:
	:noindex: