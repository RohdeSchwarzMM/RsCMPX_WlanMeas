StbCoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:STBCoding

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:STBCoding



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Htsig.StbCoding.StbCodingCls
	:members:
	:undoc-members:
	:noindex: