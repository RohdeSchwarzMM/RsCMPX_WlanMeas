Parity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:PARity

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:PARity



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Lsig.Parity.ParityCls
	:members:
	:undoc-members:
	:noindex: