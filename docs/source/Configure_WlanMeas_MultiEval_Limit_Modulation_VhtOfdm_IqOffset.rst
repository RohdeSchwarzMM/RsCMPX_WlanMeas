IqOffset
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.VhtOfdm.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.modulation.vhtOfdm.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_Modulation_VhtOfdm_IqOffset_Bw.rst