Tmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ABORt:WLAN:MEASurement<Instance>:TMODe

.. code-block:: python

	ABORt:WLAN:MEASurement<Instance>:TMODe



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.tmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_Tmode_Antenna.rst
	WlanMeas_Tmode_Data.rst