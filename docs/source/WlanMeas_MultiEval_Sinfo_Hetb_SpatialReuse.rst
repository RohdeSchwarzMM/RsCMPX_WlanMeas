SpatialReuse<Spatial>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.wlanMeas.multiEval.sinfo.hetb.spatialReuse.repcap_spatial_get()
	driver.wlanMeas.multiEval.sinfo.hetb.spatialReuse.repcap_spatial_set(repcap.Spatial.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:SPATialreuse<index>

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:SPATialreuse<index>



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hetb.SpatialReuse.SpatialReuseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.hetb.spatialReuse.clone()