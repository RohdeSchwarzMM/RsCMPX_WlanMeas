Terror
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.Terror.TerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.terror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_Terror_Mimo.rst