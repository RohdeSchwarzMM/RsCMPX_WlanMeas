Sreliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:SRELiability

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:SRELiability



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Sreliability.SreliabilityCls
	:members:
	:undoc-members:
	:noindex: