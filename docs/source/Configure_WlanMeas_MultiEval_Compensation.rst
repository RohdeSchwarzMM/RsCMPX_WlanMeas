Compensation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:CESTimation
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:SMOothing
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:NCANcel

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:CESTimation
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:SMOothing
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:NCANcel



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Compensation.CompensationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.compensation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Compensation_EfTaps.rst
	Configure_WlanMeas_MultiEval_Compensation_SkipSymbols.rst
	Configure_WlanMeas_MultiEval_Compensation_Tracking.rst