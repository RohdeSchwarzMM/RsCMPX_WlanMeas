Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: