Umargin<Connector>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.wlanMeas.rfSettings.umargin.repcap_connector_get()
	driver.configure.wlanMeas.rfSettings.umargin.repcap_connector_set(repcap.Connector.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:UMARgin<antenna>

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:RFSettings:UMARgin<antenna>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.RfSettings.Umargin.UmarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.rfSettings.umargin.clone()