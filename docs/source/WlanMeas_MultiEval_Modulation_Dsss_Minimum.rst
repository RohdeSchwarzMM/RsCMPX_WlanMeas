Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Dsss.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: