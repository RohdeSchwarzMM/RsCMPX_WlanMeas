Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Segments.Frequency.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: