Stbc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:STBC

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:STBC



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hesu.Stbc.StbcCls
	:members:
	:undoc-members:
	:noindex: