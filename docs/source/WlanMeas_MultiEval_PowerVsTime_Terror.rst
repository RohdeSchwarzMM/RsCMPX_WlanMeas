Terror
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.TerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.powerVsTime.terror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_PowerVsTime_Terror_Average.rst
	WlanMeas_MultiEval_PowerVsTime_Terror_Current.rst
	WlanMeas_MultiEval_PowerVsTime_Terror_Maximum.rst
	WlanMeas_MultiEval_PowerVsTime_Terror_Mimo.rst
	WlanMeas_MultiEval_PowerVsTime_Terror_Minimum.rst
	WlanMeas_MultiEval_PowerVsTime_Terror_StandardDev.rst