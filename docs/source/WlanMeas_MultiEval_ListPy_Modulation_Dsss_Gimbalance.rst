Gimbalance
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.Gimbalance.GimbalanceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dsss.gimbalance.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Gimbalance_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Gimbalance_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Gimbalance_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Gimbalance_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Gimbalance_StandardDev.rst