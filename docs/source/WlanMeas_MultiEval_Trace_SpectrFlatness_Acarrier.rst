Acarrier
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Acarrier.AcarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.spectrFlatness.acarrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_SpectrFlatness_Acarrier_Average.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Acarrier_Current.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Acarrier_Maximum.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Acarrier_Minimum.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Acarrier_Segment.rst