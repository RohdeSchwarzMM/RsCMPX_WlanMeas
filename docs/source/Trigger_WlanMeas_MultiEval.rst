MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WLAN:MEASurement<Instance>:MEValuation:SOURce
	single: TRIGger:WLAN:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:WLAN:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:WLAN:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:WLAN:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:WLAN:MEASurement<Instance>:MEValuation:OFFSet

.. code-block:: python

	TRIGger:WLAN:MEASurement<Instance>:MEValuation:SOURce
	TRIGger:WLAN:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:WLAN:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:WLAN:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:WLAN:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:WLAN:MEASurement<Instance>:MEValuation:OFFSet



.. autoclass:: RsCMPX_WlanMeas.Implementations.Trigger.WlanMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wlanMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WlanMeas_MultiEval_Catalog.rst