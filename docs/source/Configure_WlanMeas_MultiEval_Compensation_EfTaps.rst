EfTaps
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:EFTaps

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:EFTaps



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Compensation.EfTaps.EfTapsCls
	:members:
	:undoc-members:
	:noindex: