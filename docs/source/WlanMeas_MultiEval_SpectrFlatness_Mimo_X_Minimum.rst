Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MINimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MINimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.X.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: