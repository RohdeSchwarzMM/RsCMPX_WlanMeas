Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Segments.Frequency.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: