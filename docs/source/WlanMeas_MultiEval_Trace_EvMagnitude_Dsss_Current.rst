Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:DSSS:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:DSSS:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:DSSS:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:DSSS:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Dsss.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: