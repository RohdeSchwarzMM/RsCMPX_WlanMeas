Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:CURRent
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:CURRent

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:CURRent
	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.RisingEdge.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: