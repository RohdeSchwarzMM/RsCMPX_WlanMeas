Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Ofdm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: