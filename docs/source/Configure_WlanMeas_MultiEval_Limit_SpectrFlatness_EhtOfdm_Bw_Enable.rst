Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:SFLatness:EHTofdm:BW<bandwidth>:ENABle

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:SFLatness:EHTofdm:BW<bandwidth>:ENABle



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.SpectrFlatness.EhtOfdm.Bw.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: