Tail
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:TAIL

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:TAIL



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Lsig.Tail.TailCls
	:members:
	:undoc-members:
	:noindex: