Trace
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_CfError.rst
	WlanMeas_MultiEval_Trace_EvMagnitude.rst
	WlanMeas_MultiEval_Trace_IqConstant.rst
	WlanMeas_MultiEval_Trace_PowerVsTime.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness.rst
	WlanMeas_MultiEval_Trace_Terror.rst
	WlanMeas_MultiEval_Trace_TsMask.rst