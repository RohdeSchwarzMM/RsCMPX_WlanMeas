Dsss
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Dsss.DsssCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.evMagnitude.dsss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_EvMagnitude_Dsss_Average.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Dsss_Current.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Dsss_Maximum.rst