Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency:SCHannel
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency:BAND
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency:SCHannel
	CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency:BAND
	CONFigure:WLAN:MEASurement<Instance>:RFSettings:FREQuency



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.RfSettings.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.rfSettings.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_RfSettings_Frequency_Channels.rst