Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:SEGMent<seg>:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:SEGMent<seg>:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:SEGMent<seg>:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:SEGMent<seg>:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.Segment.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: