Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Dsss.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: