Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:CURRent
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:CURRent
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Mimo.Segments.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: