Segments
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Mimo.Segments.SegmentsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.tsMask.mimo.segments.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_TsMask_Mimo_Segments_Average.rst
	WlanMeas_MultiEval_TsMask_Mimo_Segments_Current.rst
	WlanMeas_MultiEval_TsMask_Mimo_Segments_Frequency.rst
	WlanMeas_MultiEval_TsMask_Mimo_Segments_Maximum.rst
	WlanMeas_MultiEval_TsMask_Mimo_Segments_Minimum.rst