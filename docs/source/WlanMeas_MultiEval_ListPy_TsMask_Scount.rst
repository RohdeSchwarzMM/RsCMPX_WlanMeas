Scount
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:TSMask:SCOunt

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:TSMask:SCOunt



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.TsMask.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: