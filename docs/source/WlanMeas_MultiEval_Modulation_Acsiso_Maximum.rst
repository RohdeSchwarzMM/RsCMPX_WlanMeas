Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Acsiso.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: