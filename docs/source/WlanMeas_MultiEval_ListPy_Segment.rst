Segment<SegmentB>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.wlanMeas.multiEval.listPy.segment.repcap_segmentB_get()
	driver.wlanMeas.multiEval.listPy.segment.repcap_segmentB_set(repcap.SegmentB.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Segment_Modulation.rst
	WlanMeas_MultiEval_ListPy_Segment_TsMask.rst