Frequency
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.tsMask.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_TsMask_Frequency_Average.rst
	WlanMeas_MultiEval_TsMask_Frequency_Current.rst
	WlanMeas_MultiEval_TsMask_Frequency_Maximum.rst
	WlanMeas_MultiEval_TsMask_Frequency_Minimum.rst