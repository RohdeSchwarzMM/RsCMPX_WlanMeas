Mimo<Mimo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.trace.terror.mimo.repcap_mimo_get()
	driver.wlanMeas.multiEval.trace.terror.mimo.repcap_mimo_set(repcap.Mimo.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor:MIMO<n>
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor:MIMO<n>

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor:MIMO<n>
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TERRor:MIMO<n>



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.Terror.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.terror.mimo.clone()