Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:MCS

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:MCS



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hesu.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: