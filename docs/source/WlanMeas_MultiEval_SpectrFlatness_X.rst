X
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.X.XCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.spectrFlatness.x.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_SpectrFlatness_X_Average.rst
	WlanMeas_MultiEval_SpectrFlatness_X_Current.rst
	WlanMeas_MultiEval_SpectrFlatness_X_Maximum.rst
	WlanMeas_MultiEval_SpectrFlatness_X_Minimum.rst