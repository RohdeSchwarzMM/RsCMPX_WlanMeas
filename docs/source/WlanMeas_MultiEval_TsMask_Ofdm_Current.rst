Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Ofdm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: