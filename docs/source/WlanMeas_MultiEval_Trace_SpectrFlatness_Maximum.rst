Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:MAXimum

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: