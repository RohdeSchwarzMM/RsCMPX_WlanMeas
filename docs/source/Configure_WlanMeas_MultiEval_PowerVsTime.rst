PowerVsTime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:PVTime:RPOWer
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:PVTime:ALENgth
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:PVTime:BURSt

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:MEValuation:PVTime:RPOWer
	CONFigure:WLAN:MEASurement<instance>:MEValuation:PVTime:ALENgth
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:PVTime:BURSt



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex: