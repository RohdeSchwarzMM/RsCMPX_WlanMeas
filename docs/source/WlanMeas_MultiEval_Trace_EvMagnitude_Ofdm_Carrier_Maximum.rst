Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:CARRier:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:CARRier:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:CARRier:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:CARRier:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Ofdm.Carrier.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: