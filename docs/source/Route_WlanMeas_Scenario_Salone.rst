Salone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:MEASurement<Instance>:SCENario:SALone

.. code-block:: python

	ROUTe:WLAN:MEASurement<Instance>:SCENario:SALone



.. autoclass:: RsCMPX_WlanMeas.Implementations.Route.WlanMeas.Scenario.Salone.SaloneCls
	:members:
	:undoc-members:
	:noindex: