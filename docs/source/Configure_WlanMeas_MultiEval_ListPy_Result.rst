Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:RESult:MODulation
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:RESult:TSMask

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:RESult:MODulation
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:RESult:TSMask



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: