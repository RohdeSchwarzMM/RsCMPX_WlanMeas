StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:SDEViation
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:SDEViation
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:SDEViation
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:SDEViation
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Dsss.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: