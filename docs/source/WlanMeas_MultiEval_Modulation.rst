Modulation
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_Acsiso.rst
	WlanMeas_MultiEval_Modulation_Average.rst
	WlanMeas_MultiEval_Modulation_CfoDistribution.rst
	WlanMeas_MultiEval_Modulation_Cmimo.rst
	WlanMeas_MultiEval_Modulation_Current.rst
	WlanMeas_MultiEval_Modulation_Dsss.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude.rst
	WlanMeas_MultiEval_Modulation_Maximum.rst
	WlanMeas_MultiEval_Modulation_Mimo.rst
	WlanMeas_MultiEval_Modulation_Minimum.rst
	WlanMeas_MultiEval_Modulation_Ofdm.rst
	WlanMeas_MultiEval_Modulation_Segments.rst
	WlanMeas_MultiEval_Modulation_Smimo.rst
	WlanMeas_MultiEval_Modulation_StandardDev.rst