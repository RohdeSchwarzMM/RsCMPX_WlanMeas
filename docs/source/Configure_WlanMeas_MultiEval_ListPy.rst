ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:COUNt
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:CMODe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:STIMe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:MTIMe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:MOFFset
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:ENPower
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:FREQuency
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:STANdard
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:BWIDth
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:BTYPe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:RTRigger
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:COUNt
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:CMODe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:STIMe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:MTIMe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:MOFFset
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:ENPower
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:FREQuency
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:STANdard
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:BWIDth
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:BTYPe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:RTRigger
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_ListPy_Result.rst
	Configure_WlanMeas_MultiEval_ListPy_Scount.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment.rst
	Configure_WlanMeas_MultiEval_ListPy_SingleCmw.rst