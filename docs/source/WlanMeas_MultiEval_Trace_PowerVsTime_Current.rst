Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: