Channel<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.wlanMeas.multiEval.sinfo.heb.channel.repcap_channel_get()
	driver.wlanMeas.multiEval.sinfo.heb.channel.repcap_channel_set(repcap.Channel.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Heb.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.heb.channel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Heb_Channel_Cfield.rst
	WlanMeas_MultiEval_Sinfo_Heb_Channel_Ufield.rst