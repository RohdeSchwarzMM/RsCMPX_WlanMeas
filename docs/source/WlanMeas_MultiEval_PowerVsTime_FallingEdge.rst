FallingEdge
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.FallingEdge.FallingEdgeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.powerVsTime.fallingEdge.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_PowerVsTime_FallingEdge_Average.rst
	WlanMeas_MultiEval_PowerVsTime_FallingEdge_Current.rst
	WlanMeas_MultiEval_PowerVsTime_FallingEdge_Maximum.rst