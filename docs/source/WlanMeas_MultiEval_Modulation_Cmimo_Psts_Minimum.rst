Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MINimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MINimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Psts.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: