Tmimo<TrueMimoPath>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Count1 .. Count4
	rc = driver.route.wlanMeas.scenario.tmimo.repcap_trueMimoPath_get()
	driver.route.wlanMeas.scenario.tmimo.repcap_trueMimoPath_set(repcap.TrueMimoPath.Count1)



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:MEASurement<Instance>:SCENario:TMIMo<PathCount>

.. code-block:: python

	ROUTe:WLAN:MEASurement<Instance>:SCENario:TMIMo<PathCount>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Route.WlanMeas.Scenario.Tmimo.TmimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wlanMeas.scenario.tmimo.clone()