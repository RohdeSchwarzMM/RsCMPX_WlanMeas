Uinfo<User>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr144
	rc = driver.wlanMeas.multiEval.ofdma.uinfo.repcap_user_get()
	driver.wlanMeas.multiEval.ofdma.uinfo.repcap_user_set(repcap.User.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:OFDMa:UINFo<user>

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:OFDMa:UINFo<user>



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Ofdma.Uinfo.UinfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.ofdma.uinfo.clone()