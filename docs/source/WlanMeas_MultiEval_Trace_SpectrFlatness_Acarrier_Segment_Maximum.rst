Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:SEGMent<seg>:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:SEGMent<seg>:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:SEGMent<seg>:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:SEGMent<seg>:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:SEGMent<seg>:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:SEGMent<seg>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Acarrier.Segment.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: