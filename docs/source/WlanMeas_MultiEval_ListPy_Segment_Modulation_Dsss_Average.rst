Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:MODulation:DSSS:AVERage

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:MODulation:DSSS:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Segment.Modulation.Dsss.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: