Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Psts.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: