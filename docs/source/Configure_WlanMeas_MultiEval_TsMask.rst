TsMask
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:AFFTnum
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:TROTime
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBWPercent
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:MSELection
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:DMODe

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:AFFTnum
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:TROTime
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBWPercent
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:MSELection
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:TSMask:DMODe



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.TsMask.TsMaskCls
	:members:
	:undoc-members:
	:noindex: