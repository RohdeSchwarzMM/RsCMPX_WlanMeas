Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: