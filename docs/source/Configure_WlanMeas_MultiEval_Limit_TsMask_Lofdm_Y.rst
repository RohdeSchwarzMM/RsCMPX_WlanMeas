Y
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:A
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:B
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:C
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:D

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:A
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:B
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:C
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:TSMask:LOFDm:Y:D



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.Lofdm.Y.YCls
	:members:
	:undoc-members:
	:noindex: