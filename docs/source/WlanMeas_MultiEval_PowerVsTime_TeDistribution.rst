TeDistribution
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TEDistrib
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TEDistrib
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TEDistrib

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TEDistrib
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TEDistrib
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TEDistrib



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.TeDistribution.TeDistributionCls
	:members:
	:undoc-members:
	:noindex: