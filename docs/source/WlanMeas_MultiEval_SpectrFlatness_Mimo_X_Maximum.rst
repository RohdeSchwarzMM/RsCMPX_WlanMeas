Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.X.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: