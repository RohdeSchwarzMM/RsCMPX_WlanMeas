Crc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:CRC

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:CRC



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.VhtSig.Crc.CrcCls
	:members:
	:undoc-members:
	:noindex: