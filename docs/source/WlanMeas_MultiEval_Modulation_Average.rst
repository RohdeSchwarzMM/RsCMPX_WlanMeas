Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: