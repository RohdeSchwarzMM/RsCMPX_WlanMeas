Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MIMO<n>:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MIMO<n>:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MIMO<n>:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:MIMO<n>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.FallingEdge.Mimo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: