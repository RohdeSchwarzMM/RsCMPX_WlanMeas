Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:MIMO<n>:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:MIMO<n>:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:MIMO<n>:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:MIMO<n>:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.RisingEdge.Mimo.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: