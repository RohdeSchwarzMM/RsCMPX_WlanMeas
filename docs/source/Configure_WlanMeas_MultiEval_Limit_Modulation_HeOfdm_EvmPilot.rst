EvmPilot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:HEOFdm:EVMPilot:TBHigh
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:HEOFdm:EVMPilot:TBLow
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:HEOFdm:EVMPilot

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:HEOFdm:EVMPilot:TBHigh
	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:HEOFdm:EVMPilot:TBLow
	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:MODulation:HEOFdm:EVMPilot



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.HeOfdm.EvmPilot.EvmPilotCls
	:members:
	:undoc-members:
	:noindex: