StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: