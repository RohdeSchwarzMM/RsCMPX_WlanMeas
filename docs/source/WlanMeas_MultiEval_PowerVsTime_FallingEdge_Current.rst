Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:CURRent
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:CURRent

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:CURRent
	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:FEDGe:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.FallingEdge.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: