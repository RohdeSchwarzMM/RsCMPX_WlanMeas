RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:SANTennas
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:MLOFfset
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:LRINterval

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:RFSettings:SANTennas
	CONFigure:WLAN:MEASurement<Instance>:RFSettings:MLOFfset
	CONFigure:WLAN:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:WLAN:MEASurement<Instance>:RFSettings:LRINterval



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_RfSettings_Antenna.rst
	Configure_WlanMeas_RfSettings_Eattenuation.rst
	Configure_WlanMeas_RfSettings_EnvelopePower.rst
	Configure_WlanMeas_RfSettings_Frequency.rst
	Configure_WlanMeas_RfSettings_LrStart.rst
	Configure_WlanMeas_RfSettings_Umargin.rst