Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:MINimum

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: