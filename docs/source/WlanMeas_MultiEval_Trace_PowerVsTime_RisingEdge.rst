RisingEdge
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.RisingEdge.RisingEdgeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.powerVsTime.risingEdge.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_PowerVsTime_RisingEdge_Average.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_RisingEdge_Current.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_RisingEdge_Maximum.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_RisingEdge_Mimo.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_RisingEdge_Minimum.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_RisingEdge_Segment.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_RisingEdge_Time.rst