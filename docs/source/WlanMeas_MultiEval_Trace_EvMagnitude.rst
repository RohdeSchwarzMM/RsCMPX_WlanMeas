EvMagnitude
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_EvMagnitude_Acsiso.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Carrier.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Dsss.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Nsiso.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Ofdm.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Symbol.rst