Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:SEGMent<seg>:TIME
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:SEGMent<seg>:TIME

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:SEGMent<seg>:TIME
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:SEGMent<seg>:TIME



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.RisingEdge.Segment.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: