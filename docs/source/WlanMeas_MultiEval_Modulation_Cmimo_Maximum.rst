Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: