Acsiso
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Acsiso.AcsisoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.acsiso.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_Acsiso_Average.rst
	WlanMeas_MultiEval_Modulation_Acsiso_Current.rst
	WlanMeas_MultiEval_Modulation_Acsiso_Maximum.rst
	WlanMeas_MultiEval_Modulation_Acsiso_StandardDev.rst