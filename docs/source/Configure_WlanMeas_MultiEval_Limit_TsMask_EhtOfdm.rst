EhtOfdm
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.EhtOfdm.EhtOfdmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.tsMask.ehtOfdm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_TsMask_EhtOfdm_Bw.rst