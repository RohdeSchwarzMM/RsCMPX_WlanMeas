Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Ofdm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: