Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:CURRent
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:CURRent
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Psts.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: