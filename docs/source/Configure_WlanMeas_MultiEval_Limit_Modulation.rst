Modulation
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_Modulation_Dsss.rst
	Configure_WlanMeas_MultiEval_Limit_Modulation_EhtOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_Modulation_HeOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_Modulation_HtOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_Modulation_Lofdm.rst
	Configure_WlanMeas_MultiEval_Limit_Modulation_Pofdm.rst
	Configure_WlanMeas_MultiEval_Limit_Modulation_VhtOfdm.rst