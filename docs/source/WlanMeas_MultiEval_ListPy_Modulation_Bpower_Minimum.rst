Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:MINimum

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Bpower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: