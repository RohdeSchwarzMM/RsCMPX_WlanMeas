Paid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:PAID

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:PAID



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.VhtSig.Paid.PaidCls
	:members:
	:undoc-members:
	:noindex: