Crc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:CRC

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HETB:CRC



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hetb.Crc.CrcCls
	:members:
	:undoc-members:
	:noindex: