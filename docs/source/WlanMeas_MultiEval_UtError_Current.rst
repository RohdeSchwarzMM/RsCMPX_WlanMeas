Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: