Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Segments.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: