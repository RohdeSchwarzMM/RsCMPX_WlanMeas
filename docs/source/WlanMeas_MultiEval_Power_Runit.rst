Runit<ResourceUnit>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr144
	rc = driver.wlanMeas.multiEval.power.runit.repcap_resourceUnit_get()
	driver.wlanMeas.multiEval.power.runit.repcap_resourceUnit_set(repcap.ResourceUnit.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.Runit.RunitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.power.runit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Power_Runit_Average.rst
	WlanMeas_MultiEval_Power_Runit_Current.rst
	WlanMeas_MultiEval_Power_Runit_Maximum.rst
	WlanMeas_MultiEval_Power_Runit_RxAntenna.rst
	WlanMeas_MultiEval_Power_Runit_StandardDev.rst