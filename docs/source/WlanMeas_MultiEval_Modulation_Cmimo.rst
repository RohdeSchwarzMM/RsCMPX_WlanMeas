Cmimo
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.CmimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.cmimo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_Cmimo_Average.rst
	WlanMeas_MultiEval_Modulation_Cmimo_Current.rst
	WlanMeas_MultiEval_Modulation_Cmimo_Maximum.rst
	WlanMeas_MultiEval_Modulation_Cmimo_Psts.rst
	WlanMeas_MultiEval_Modulation_Cmimo_StandardDev.rst