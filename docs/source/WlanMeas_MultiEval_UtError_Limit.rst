Limit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:LIMit
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:LIMit
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:LIMit

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:LIMit
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:LIMit
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:LIMit



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex: