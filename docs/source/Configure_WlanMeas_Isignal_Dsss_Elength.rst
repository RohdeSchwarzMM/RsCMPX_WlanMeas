Elength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:DSSS:ELENgth

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:DSSS:ELENgth



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Isignal.Dsss.Elength.ElengthCls
	:members:
	:undoc-members:
	:noindex: