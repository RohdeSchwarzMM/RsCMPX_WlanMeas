Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: