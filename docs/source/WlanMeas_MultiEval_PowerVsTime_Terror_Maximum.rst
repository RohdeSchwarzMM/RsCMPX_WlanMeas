Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: