TsMask
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.TsMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.tsMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_TsMask_Dsss.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_EhtOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_HeOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_HtOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_Lofdm.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_Pofdm.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_VhtOfdm.rst