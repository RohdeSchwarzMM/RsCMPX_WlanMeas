StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:SDEViation
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:SDEViation
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:SDEViation
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:SDEViation
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SEGMents:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Segments.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: