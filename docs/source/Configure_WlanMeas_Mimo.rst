Mimo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:MIMO:NOANtennas

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:MIMO:NOANtennas



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex: