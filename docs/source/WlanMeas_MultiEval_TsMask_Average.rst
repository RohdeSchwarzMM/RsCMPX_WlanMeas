Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: