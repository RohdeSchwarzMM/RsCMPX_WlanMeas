Upper
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:SFLatness:EHTofdm:BW<bandwidth>:UPPer

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:SFLatness:EHTofdm:BW<bandwidth>:UPPer



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.SpectrFlatness.EhtOfdm.Bw.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex: