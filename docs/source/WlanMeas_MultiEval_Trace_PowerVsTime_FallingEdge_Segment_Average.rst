Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:SEGMent<seg>:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:SEGMent<seg>:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:SEGMent<seg>:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:FEDGe:SEGMent<seg>:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.FallingEdge.Segment.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: