Cbw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:CBW

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:CBW



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Htsig.Cbw.CbwCls
	:members:
	:undoc-members:
	:noindex: