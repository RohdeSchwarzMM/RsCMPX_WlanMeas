Mask
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MASK
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MASK

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MASK
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MASK



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.Mask.MaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.tsMask.mask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_TsMask_Mask_Mimo.rst
	WlanMeas_MultiEval_Trace_TsMask_Mask_Segment.rst