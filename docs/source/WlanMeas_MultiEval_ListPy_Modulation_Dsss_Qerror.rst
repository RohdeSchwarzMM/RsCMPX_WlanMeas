Qerror
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.Qerror.QerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dsss.qerror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Qerror_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Qerror_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Qerror_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Qerror_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_Qerror_StandardDev.rst