Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Acsiso.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: