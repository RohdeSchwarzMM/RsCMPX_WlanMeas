Rtrigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:RTRigger

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIST:SEGMent<segment>:RTRigger



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.Segment.Rtrigger.RtriggerCls
	:members:
	:undoc-members:
	:noindex: