Ofdm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:OFDM:ELENgth

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:OFDM:ELENgth



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Isignal.Ofdm.OfdmCls
	:members:
	:undoc-members:
	:noindex: