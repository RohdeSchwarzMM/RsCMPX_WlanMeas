Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Segments.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: