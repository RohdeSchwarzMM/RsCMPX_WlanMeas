Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:FREQuency:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Segments.Frequency.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: