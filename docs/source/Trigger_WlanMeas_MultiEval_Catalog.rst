Catalog
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.Trigger.WlanMeas.MultiEval.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wlanMeas.multiEval.catalog.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_WlanMeas_MultiEval_Catalog_Source.rst