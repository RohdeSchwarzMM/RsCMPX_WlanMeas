Bw<BandwidthC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw40
	rc = driver.configure.wlanMeas.multiEval.limit.modulation.htOfdm.iqOffset.bw.repcap_bandwidthC_get()
	driver.configure.wlanMeas.multiEval.limit.modulation.htOfdm.iqOffset.bw.repcap_bandwidthC_set(repcap.BandwidthC.Bw5)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:HTOFdm:IQOFfset:BW<BW>

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:MODulation:HTOFdm:IQOFfset:BW<BW>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.Modulation.HtOfdm.IqOffset.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.modulation.htOfdm.iqOffset.bw.clone()