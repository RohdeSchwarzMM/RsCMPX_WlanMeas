Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:DSSS:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Dsss.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: