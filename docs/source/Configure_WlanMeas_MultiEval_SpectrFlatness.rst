SpectrFlatness
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:SFLatness:DMODe

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:SFLatness:DMODe



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.SpectrFlatness.SpectrFlatnessCls
	:members:
	:undoc-members:
	:noindex: