Bpower
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Bpower.BpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.bpower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Bpower_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Bpower_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Bpower_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Bpower_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Bpower_StandardDev.rst