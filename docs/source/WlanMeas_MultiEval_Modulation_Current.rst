Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: