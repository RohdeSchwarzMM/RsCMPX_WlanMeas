Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Frequency.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: