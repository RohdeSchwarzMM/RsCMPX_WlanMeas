Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:CURRent

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: