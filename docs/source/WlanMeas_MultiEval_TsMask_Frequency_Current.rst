Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Frequency.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: