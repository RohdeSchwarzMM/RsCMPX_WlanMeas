Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Ofdm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: