Enums
=========

Bandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Bandwidth.BW05mhz
	# All values (8x):
	BW05mhz | BW10mhz | BW16mhz | BW20mhz | BW32mhz | BW40mhz | BW80mhz | BW88mhz

BurstEvalLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BurstEvalLength.REDucedburst
	# All values (2x):
	REDucedburst | WHOLeburst

BurstType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BurstType.AUTO
	# All values (4x):
	AUTO | DLIN | GREenfield | MIXed

BurstTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BurstTypeB.GREenfield
	# All values (2x):
	GREenfield | MIXed

CfoEstimation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CfoEstimation.FULLpacket
	# All values (2x):
	FULLpacket | PREamble

ChannelEstimation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelEstimation.PAYLoad
	# All values (2x):
	PAYLoad | PREamble

Coderate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Coderate.AUTO
	# All values (7x):
	AUTO | CR12 | CR14dcm | CR23 | CR34 | CR38dcm | CR56

CodingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingType.BCC
	# All values (2x):
	BCC | LDPC

ConnectorSwitch
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ConnectorSwitch.R11
	# Last value:
	value = enums.ConnectorSwitch.RH8
	# All values (96x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8
	RC1 | RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8
	RD1 | RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8
	RE1 | RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8
	RF1 | RF2 | RF3 | RF4 | RF5 | RF6 | RF7 | RF8
	RG1 | RG2 | RG3 | RG4 | RG5 | RG6 | RG7 | RG8
	RH1 | RH2 | RH3 | RH4 | RH5 | RH6 | RH7 | RH8

ConnectorSwitchExt
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ConnectorSwitchExt.OFF
	# Last value:
	value = enums.ConnectorSwitchExt.RH8
	# All values (98x):
	OFF | ON | R11 | R12 | R13 | R14 | R15 | R16
	R17 | R18 | R21 | R22 | R23 | R24 | R25 | R26
	R27 | R28 | R31 | R32 | R33 | R34 | R35 | R36
	R37 | R38 | R41 | R42 | R43 | R44 | R45 | R46
	R47 | R48 | RA1 | RA2 | RA3 | RA4 | RA5 | RA6
	RA7 | RA8 | RB1 | RB2 | RB3 | RB4 | RB5 | RB6
	RB7 | RB8 | RC1 | RC2 | RC3 | RC4 | RC5 | RC6
	RC7 | RC8 | RD1 | RD2 | RD3 | RD4 | RD5 | RD6
	RD7 | RD8 | RE1 | RE2 | RE3 | RE4 | RE5 | RE6
	RE7 | RE8 | RF1 | RF2 | RF3 | RF4 | RF5 | RF6
	RF7 | RF8 | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

ConnectorTuple
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectorTuple.CT12
	# All values (7x):
	CT12 | CT14 | CT18 | CT34 | CT56 | CT58 | CT78

DecodeStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DecodeStatus.INV
	# All values (3x):
	INV | NAV | OK

DisplayMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DisplayMode.ABSolute
	# All values (2x):
	ABSolute | RELative

EvmMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EvmMethod.ST1999
	# All values (3x):
	ST1999 | ST2007 | ST2016

FftOffset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FftOffset.AUTO
	# All values (3x):
	AUTO | CENT | PEAK

FrequencyBand
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrequencyBand.B24Ghz
	# All values (4x):
	B24Ghz | B4GHz | B5GHz | B6GHz

GuardInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GuardInterval.GI08
	# All values (5x):
	GI08 | GI16 | GI32 | LONG | SHORt

GuiScenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GuiScenario.CSPath
	# All values (8x):
	CSPath | MIMO2x2 | MIMO4x4 | MIMO8x8 | SALone | SMI4 | TMIMo | UNDefined

IeeeStandard
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IeeeStandard.DSSS
	# All values (7x):
	DSSS | EHTofdm | HEOFdm | HTOFdm | LOFDm | POFDm | VHTofdm

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

LtfSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LtfSize.LTF1
	# All values (3x):
	LTF1 | LTF2 | LTF4

MimoScenario
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MimoScenario.CSPath
	# Last value:
	value = enums.MimoScenario.UNDefined
	# All values (10x):
	CSPath | MIMO2x2 | MIMO4x4 | MIMO8x8 | SALone | SMI4 | TMIM2x2 | TMIM3x3
	TMIM4x4 | UNDefined

ModulationFilter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ModulationFilter.ALL
	# Last value:
	value = enums.ModulationFilter.QPSK
	# All values (11x):
	ALL | BPSK | CCK11 | CCK5_5 | DBPSk | DQPSk | QAM1024 | QAM16
	QAM256 | QAM64 | QPSK

ModulationTypeB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ModulationTypeB.BP1_5
	# Last value:
	value = enums.ModulationTypeB.QR34
	# All values (32x):
	BP1_5 | BP2_25 | BP3 | BP4_5 | BPM6 | BPM9 | BR12 | Q1M12
	Q1M18 | Q1M24 | Q1M36 | Q1M6 | Q1M9 | Q1R12 | Q1R34 | Q6M12
	Q6M135 | Q6M24 | Q6M27 | Q6M48 | Q6M54 | Q6R23 | Q6R34 | Q6R56
	QM12 | QM18 | QM3 | QM4_5 | QM6 | QM9 | QR12 | QR34

ModulationTypeC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationTypeC.CCK11
	# All values (4x):
	CCK11 | CCK5 | DBPSk1 | DQPSk2

ModulationTypeD
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ModulationTypeD._16Q
	# Last value:
	value = enums.ModulationTypeD.UNSPecified
	# All values (28x):
	_16Q | _16Q12 | _16Q14 | _16Q34 | _16Q38 | _1KQ | _1KQ34 | _1KQ56
	_256Q | _256Q34 | _256Q56 | _4KQ | _4KQ34 | _4KQ56 | _64Q | _64Q12
	_64Q23 | _64Q34 | _64Q56 | BPSK | BPSK12 | BPSK14 | BPSK34 | QPSK
	QPSK12 | QPSK14 | QPSK34 | UNSPecified

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

PlcpType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PlcpType.LONGplcp
	# All values (2x):
	LONGplcp | SHORtplcp

PowerClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerClass.CLA
	# All values (4x):
	CLA | CLB | CLCD | USERdefined

ReceiveMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReceiveMode.CMIMo
	# All values (4x):
	CMIMo | SISO | SMIMo | TMIMo

RefPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RefPower.MAXimum
	# All values (2x):
	MAXimum | MEAN

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (219x):
	I11I | I12O | I13I | I14O | I15I | I16O | I17I | I18O
	I21I | I22O | I23I | I24O | I25I | I26O | I27I | I28O
	I31I | I32O | I33I | I34O | I35I | I36O | I37I | I38O
	I41I | I42O | I43I | I44O | I45I | I46O | I47I | I48O
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IFO1 | IFO2
	IFO3 | IFO4 | IFO5 | IFO6 | IQ1I | IQ2O | IQ3I | IQ4O
	IQ5I | IQ6O | IQ7I | IQ8O | R10D | R11 | R118 | R1183
	R1184 | R11C | R11D | R11O | R11O3 | R11O4 | R12 | R12C
	R12D | R12I | R13 | R13C | R13O | R14 | R14C | R14I
	R15 | R16 | R17 | R18 | R21 | R214 | R218 | R21C
	R21O | R22 | R22C | R22I | R23 | R23C | R23O | R24
	R24C | R24I | R25 | R258 | R26 | R27 | R28 | R31
	R318 | R31C | R31O | R32 | R32C | R32I | R33 | R33C
	R33O | R34 | R34C | R34I | R35 | R36 | R37 | R38
	R41 | R418 | R41C | R41O | R42 | R42C | R42I | R43
	R43C | R43O | R44 | R44C | R44I | R45 | R46 | R47
	R48 | RA1 | RA18 | RA2 | RA3 | RA4 | RA5 | RA6
	RA7 | RA8 | RB1 | RB14 | RB18 | RB2 | RB3 | RB4
	RB5 | RB6 | RB7 | RB8 | RC1 | RC18 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD18 | RD2
	RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE18
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF18 | RF1C | RF1O | RF2 | RF2C | RF2I | RF3 | RF3C
	RF3O | RF4 | RF4C | RF4I | RF5 | RF5C | RF6 | RF6C
	RF7 | RF7C | RF8 | RF8C | RF9C | RFAC | RFAO | RFBC
	RFBI | RG1 | RG18 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH18 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

RxConnectorExt
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnectorExt.I11I
	# Last value:
	value = enums.RxConnectorExt.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

RxTxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxTxConverter.IRX1
	# Last value:
	value = enums.RxTxConverter.TX44
	# All values (80x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | ITX1 | ITX11 | ITX12 | ITX13
	ITX14 | ITX2 | ITX21 | ITX22 | ITX23 | ITX24 | ITX3 | ITX31
	ITX32 | ITX33 | ITX34 | ITX4 | ITX41 | ITX42 | ITX43 | ITX44
	RX1 | RX11 | RX12 | RX13 | RX14 | RX2 | RX21 | RX22
	RX23 | RX24 | RX3 | RX31 | RX32 | RX33 | RX34 | RX4
	RX41 | RX42 | RX43 | RX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SynchroMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SynchroMode.NORMal
	# All values (2x):
	NORMal | TOLerant

TrainingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrainingMode.MMODe
	# All values (2x):
	MMODe | TMODe

TriggerSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSlope.FEDGe
	# All values (4x):
	FEDGe | OFF | ON | REDGe

