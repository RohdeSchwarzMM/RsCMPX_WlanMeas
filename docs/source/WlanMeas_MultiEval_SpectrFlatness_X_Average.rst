Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.X.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: