BssColor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:BSSColor

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:BSSColor



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hemu.BssColor.BssColorCls
	:members:
	:undoc-members:
	:noindex: