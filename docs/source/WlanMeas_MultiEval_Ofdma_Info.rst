Info
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:OFDMa:INFO

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:OFDMa:INFO



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Ofdma.Info.InfoCls
	:members:
	:undoc-members:
	:noindex: