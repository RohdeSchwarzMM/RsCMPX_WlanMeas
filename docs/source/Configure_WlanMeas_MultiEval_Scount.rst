Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCOunt:TSMask
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCOunt:PVTime
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCOunt:MODulation

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCOunt:TSMask
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCOunt:PVTime
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:SCOunt:MODulation



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: