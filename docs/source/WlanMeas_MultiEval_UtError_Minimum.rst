Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: