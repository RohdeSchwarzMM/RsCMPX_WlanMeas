Acsiso
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Acsiso.AcsisoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.spectrFlatness.acsiso.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_SpectrFlatness_Acsiso_Average.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Acsiso_Current.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Acsiso_Maximum.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Acsiso_Minimum.rst