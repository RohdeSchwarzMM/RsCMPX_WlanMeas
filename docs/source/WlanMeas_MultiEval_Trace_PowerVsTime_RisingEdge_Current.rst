Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:REDGe:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.RisingEdge.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: