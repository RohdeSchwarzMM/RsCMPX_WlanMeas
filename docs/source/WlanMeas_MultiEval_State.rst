State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:STATe

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:STATe



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_State_All.rst