Y
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.TsMask.Pofdm.Bw.Ca.Y.YCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.tsMask.pofdm.bw.ca.y.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_TsMask_Pofdm_Bw_Ca_Y_A.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_Pofdm_Bw_Ca_Y_B.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_Pofdm_Bw_Ca_Y_C.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_Pofdm_Bw_Ca_Y_D.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask_Pofdm_Bw_Ca_Y_E.rst