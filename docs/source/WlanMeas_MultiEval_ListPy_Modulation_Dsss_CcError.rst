CcError
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.CcError.CcErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dsss.ccError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CcError_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CcError_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CcError_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CcError_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CcError_StandardDev.rst