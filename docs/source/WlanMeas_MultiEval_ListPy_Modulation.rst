Modulation
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Bpower.rst
	WlanMeas_MultiEval_ListPy_Modulation_Cfactor.rst
	WlanMeas_MultiEval_ListPy_Modulation_CfError.rst
	WlanMeas_MultiEval_ListPy_Modulation_DcPower.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dpower.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmAll.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmData.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmPilot.rst
	WlanMeas_MultiEval_ListPy_Modulation_Gimbalance.rst
	WlanMeas_MultiEval_ListPy_Modulation_IqOffset.rst
	WlanMeas_MultiEval_ListPy_Modulation_LtfPower.rst
	WlanMeas_MultiEval_ListPy_Modulation_Pbackoff.rst
	WlanMeas_MultiEval_ListPy_Modulation_Ppower.rst
	WlanMeas_MultiEval_ListPy_Modulation_Qerror.rst
	WlanMeas_MultiEval_ListPy_Modulation_ScError.rst
	WlanMeas_MultiEval_ListPy_Modulation_Scount.rst