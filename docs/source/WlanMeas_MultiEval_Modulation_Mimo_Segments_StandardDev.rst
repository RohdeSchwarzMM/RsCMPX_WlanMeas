StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:SDEViation
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:SDEViation
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:SDEViation
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:SDEViation
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SEGMents:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Mimo.Segments.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: