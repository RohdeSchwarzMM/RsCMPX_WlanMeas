Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MIMO<n>:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.Mimo.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: