Modulation
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.segment.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Segment_Modulation_Average.rst
	WlanMeas_MultiEval_ListPy_Segment_Modulation_Current.rst
	WlanMeas_MultiEval_ListPy_Segment_Modulation_Dsss.rst
	WlanMeas_MultiEval_ListPy_Segment_Modulation_Maximum.rst
	WlanMeas_MultiEval_ListPy_Segment_Modulation_Minimum.rst
	WlanMeas_MultiEval_ListPy_Segment_Modulation_StandardDev.rst