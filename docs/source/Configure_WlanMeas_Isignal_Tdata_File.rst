File
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:ISIGnal:TDATa:FILE:DATE

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:ISIGnal:TDATa:FILE:DATE



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Isignal.Tdata.File.FileCls
	:members:
	:undoc-members:
	:noindex: