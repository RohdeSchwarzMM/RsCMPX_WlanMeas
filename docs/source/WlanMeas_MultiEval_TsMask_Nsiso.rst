Nsiso
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Nsiso.NsisoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.tsMask.nsiso.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_TsMask_Nsiso_Average.rst
	WlanMeas_MultiEval_TsMask_Nsiso_Current.rst
	WlanMeas_MultiEval_TsMask_Nsiso_Maximum.rst