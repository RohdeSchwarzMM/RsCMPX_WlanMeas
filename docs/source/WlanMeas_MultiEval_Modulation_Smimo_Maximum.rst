Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:MAXimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:MAXimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Smimo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: