StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:SDEViation
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:SDEViation
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: