PfecPadding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:PFECpadding

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:PFECpadding



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hemu.PfecPadding.PfecPaddingCls
	:members:
	:undoc-members:
	:noindex: