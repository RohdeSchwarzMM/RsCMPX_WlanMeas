Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: