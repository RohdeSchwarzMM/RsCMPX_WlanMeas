FallingEdge
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.FallingEdge.FallingEdgeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.powerVsTime.fallingEdge.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_PowerVsTime_FallingEdge_Average.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_FallingEdge_Current.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_FallingEdge_Maximum.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_FallingEdge_Mimo.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_FallingEdge_Minimum.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_FallingEdge_Segment.rst
	WlanMeas_MultiEval_Trace_PowerVsTime_FallingEdge_Time.rst