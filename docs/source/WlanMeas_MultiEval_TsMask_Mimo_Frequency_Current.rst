Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:FREQuency:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Mimo.Frequency.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: