Segments
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW:SEGMents
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW:SEGMents

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW:SEGMents
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OBW:SEGMents



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Obw.Segments.SegmentsCls
	:members:
	:undoc-members:
	:noindex: