Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MAXimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MAXimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MAXimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MAXimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: