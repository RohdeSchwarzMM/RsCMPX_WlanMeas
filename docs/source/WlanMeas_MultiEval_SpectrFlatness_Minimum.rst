Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MINimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MINimum
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MINimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MINimum
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: