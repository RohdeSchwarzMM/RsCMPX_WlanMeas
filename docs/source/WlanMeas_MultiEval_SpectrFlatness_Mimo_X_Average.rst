Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:X:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.X.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: