Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:CMIMo:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: