Reserved
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:REServed

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:LSIG:REServed



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Lsig.Reserved.ReservedCls
	:members:
	:undoc-members:
	:noindex: