Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:CMIMo:PSTS:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Cmimo.Psts.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: