Isignal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:STANdard
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:RMODe
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:ELENgth
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:BTYPe
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:BWIDth
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:CDIStance
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:PCLass
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:IQSWap
	single: CONFigure:WLAN:MEASurement<Instance>:ISIGnal:MODFilter

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:STANdard
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:RMODe
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:ELENgth
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:BTYPe
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:BWIDth
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:CDIStance
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:PCLass
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:IQSWap
	CONFigure:WLAN:MEASurement<Instance>:ISIGnal:MODFilter



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.Isignal.IsignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.isignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_Isignal_Dsss.rst
	Configure_WlanMeas_Isignal_Ofdm.rst
	Configure_WlanMeas_Isignal_Tdata.rst