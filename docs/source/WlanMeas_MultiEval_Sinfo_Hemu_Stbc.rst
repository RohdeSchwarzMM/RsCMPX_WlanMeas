Stbc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:STBC

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:STBC



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hemu.Stbc.StbcCls
	:members:
	:undoc-members:
	:noindex: