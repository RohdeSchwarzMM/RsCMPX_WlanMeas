Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:ACSiso:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Acsiso.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: