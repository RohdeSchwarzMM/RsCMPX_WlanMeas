Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Acsiso.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: