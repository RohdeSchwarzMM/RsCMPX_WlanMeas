Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:FREQuency
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:FREQuency

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:FREQuency
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:FREQuency



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: