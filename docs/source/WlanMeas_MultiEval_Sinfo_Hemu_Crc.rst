Crc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:CRC

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEMU:CRC



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hemu.Crc.CrcCls
	:members:
	:undoc-members:
	:noindex: