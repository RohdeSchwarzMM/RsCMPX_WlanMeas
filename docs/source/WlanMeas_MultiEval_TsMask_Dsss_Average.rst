Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:DSSS:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Dsss.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: