Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: