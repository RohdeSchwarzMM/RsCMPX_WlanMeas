EvmPilot
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.EvmPilot.EvmPilotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.evmPilot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_EvmPilot_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmPilot_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmPilot_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmPilot_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmPilot_StandardDev.rst