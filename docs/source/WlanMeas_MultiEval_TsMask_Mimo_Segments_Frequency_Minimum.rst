Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MIMO<n>:SEGMents:FREQuency:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Mimo.Segments.Frequency.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: