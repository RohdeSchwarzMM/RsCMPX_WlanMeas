Ofdm
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Ofdm.OfdmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.evMagnitude.ofdm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_EvMagnitude_Ofdm_Carrier.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Ofdm_Symbol.rst