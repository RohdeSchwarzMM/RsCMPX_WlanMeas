Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: