Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:AVERage

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: