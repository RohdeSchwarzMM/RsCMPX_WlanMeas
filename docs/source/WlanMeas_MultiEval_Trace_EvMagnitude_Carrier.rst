Carrier
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.evMagnitude.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_EvMagnitude_Carrier_Average.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Carrier_Current.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Carrier_Maximum.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Carrier_Mimo.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Carrier_Minimum.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Carrier_Segment.rst