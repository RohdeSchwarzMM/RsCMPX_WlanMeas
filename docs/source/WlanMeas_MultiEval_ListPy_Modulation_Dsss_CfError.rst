CfError
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.CfError.CfErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dsss.cfError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CfError_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CfError_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CfError_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CfError_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_CfError_StandardDev.rst