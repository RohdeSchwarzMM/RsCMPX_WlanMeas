Coding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEB:CHANnel<ch_index>:UFIeld<usr_index>:CODing

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HEB:CHANnel<ch_index>:UFIeld<usr_index>:CODing



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Heb.Channel.Ufield.Coding.CodingCls
	:members:
	:undoc-members:
	:noindex: