Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:AVERage
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:AVERage
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Mimo.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: