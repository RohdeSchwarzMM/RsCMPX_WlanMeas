Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: