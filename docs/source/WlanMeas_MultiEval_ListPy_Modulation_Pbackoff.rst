Pbackoff
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:PBACkoff

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:PBACkoff



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Pbackoff.PbackoffCls
	:members:
	:undoc-members:
	:noindex: