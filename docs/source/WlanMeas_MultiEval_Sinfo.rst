Sinfo
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.SinfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Heb.rst
	WlanMeas_MultiEval_Sinfo_Hemu.rst
	WlanMeas_MultiEval_Sinfo_Hesu.rst
	WlanMeas_MultiEval_Sinfo_Hetb.rst
	WlanMeas_MultiEval_Sinfo_Htsig.rst
	WlanMeas_MultiEval_Sinfo_Lsig.rst
	WlanMeas_MultiEval_Sinfo_VhtSig.rst