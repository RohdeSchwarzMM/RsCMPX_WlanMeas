Margin
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.utError.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_UtError_Margin_Average.rst
	WlanMeas_MultiEval_UtError_Margin_Current.rst
	WlanMeas_MultiEval_UtError_Margin_Maximum.rst
	WlanMeas_MultiEval_UtError_Margin_Minimum.rst