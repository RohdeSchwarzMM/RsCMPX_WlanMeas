Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:AVERage
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:AVERage
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:AVERage
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:AVERage
	CALCulate:WLAN:MEASurement<instance>:MEValuation:SFLatness:MIMO<n>:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.Mimo.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: