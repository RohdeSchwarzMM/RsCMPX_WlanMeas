Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MINimum
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MINimum
	FETCh:WLAN:MEASurement<instance>:MEValuation:SFLatness:X:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.SpectrFlatness.X.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: