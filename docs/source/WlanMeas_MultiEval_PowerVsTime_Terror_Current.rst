Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:TERRor:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.Terror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: