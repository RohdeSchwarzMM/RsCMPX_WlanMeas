Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RXANtenna<n>:MAXimum

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RXANtenna<n>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.RxAntenna.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: