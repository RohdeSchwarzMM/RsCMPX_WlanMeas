Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:NSISo:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Nsiso.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: