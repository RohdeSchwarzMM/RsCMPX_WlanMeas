StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:SDEViation
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:SDEViation
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:SDEViation
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:SDEViation
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:SMIMo:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Smimo.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: