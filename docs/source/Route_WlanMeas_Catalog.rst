Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:MEASurement<Instance>:CATalog:SCENario

.. code-block:: python

	ROUTe:WLAN:MEASurement<Instance>:CATalog:SCENario



.. autoclass:: RsCMPX_WlanMeas.Implementations.Route.WlanMeas.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: