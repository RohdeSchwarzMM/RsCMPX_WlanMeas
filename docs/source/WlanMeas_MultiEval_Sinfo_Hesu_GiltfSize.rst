GiltfSize
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:GILTfsize

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HESU:GILTfsize



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Hesu.GiltfSize.GiltfSizeCls
	:members:
	:undoc-members:
	:noindex: