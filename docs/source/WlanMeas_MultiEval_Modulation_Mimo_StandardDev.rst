StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SDEViation
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SDEViation
	single: CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SDEViation
	FETCh:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SDEViation
	CALCulate:WLAN:MEASurement<instance>:MEValuation:MODulation:MIMO<n>:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Mimo.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: