IqOffset
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dsss.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dsss.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dsss_IqOffset_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_IqOffset_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_IqOffset_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_IqOffset_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dsss_IqOffset_StandardDev.rst