Dpower
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.Dpower.DpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dpower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_Dpower_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dpower_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dpower_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dpower_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_Dpower_StandardDev.rst