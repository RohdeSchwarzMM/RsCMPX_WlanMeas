Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MIMO<n>:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MIMO<n>:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MIMO<n>:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:TSMask:MIMO<n>:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.TsMask.Mimo.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: