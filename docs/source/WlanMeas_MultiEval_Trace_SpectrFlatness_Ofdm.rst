Ofdm
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Ofdm.OfdmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.spectrFlatness.ofdm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_SpectrFlatness_Ofdm_Average.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Ofdm_Current.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Ofdm_Maximum.rst
	WlanMeas_MultiEval_Trace_SpectrFlatness_Ofdm_Minimum.rst