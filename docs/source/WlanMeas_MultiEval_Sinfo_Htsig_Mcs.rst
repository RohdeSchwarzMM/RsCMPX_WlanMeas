Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:MCS

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:MCS



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Htsig.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: