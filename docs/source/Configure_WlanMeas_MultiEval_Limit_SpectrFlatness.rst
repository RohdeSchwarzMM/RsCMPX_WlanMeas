SpectrFlatness
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.SpectrFlatness.SpectrFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.spectrFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_EhtOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_HeOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_HtOfdm.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_Lofdm.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_Pofdm.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness_VhtOfdm.rst