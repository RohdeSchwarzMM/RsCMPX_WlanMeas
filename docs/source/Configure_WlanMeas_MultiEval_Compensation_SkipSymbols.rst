SkipSymbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:SKIPsymbols

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:COMPensation:SKIPsymbols



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Compensation.SkipSymbols.SkipSymbolsCls
	:members:
	:undoc-members:
	:noindex: