Scenario
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:WLAN:MEASurement<Instance>:SCENario:CSPath
	single: ROUTe:WLAN:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:WLAN:MEASurement<Instance>:SCENario:CSPath
	ROUTe:WLAN:MEASurement<Instance>:SCENario



.. autoclass:: RsCMPX_WlanMeas.Implementations.Route.WlanMeas.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wlanMeas.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_WlanMeas_Scenario_Salone.rst
	Route_WlanMeas_Scenario_Smi.rst
	Route_WlanMeas_Scenario_Smimo.rst
	Route_WlanMeas_Scenario_Tmimo.rst