DcPower
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.DcPower.DcPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.dcPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_DcPower_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_DcPower_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_DcPower_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_DcPower_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_DcPower_StandardDev.rst