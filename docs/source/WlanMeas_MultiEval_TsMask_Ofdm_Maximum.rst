Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:OFDM:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Ofdm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: