Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Ofdm.Symbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: