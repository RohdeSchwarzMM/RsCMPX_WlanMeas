StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RXANtenna<n>:SDEViation

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RXANtenna<n>:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.RxAntenna.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: