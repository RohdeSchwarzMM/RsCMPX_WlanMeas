Segment<SegmentB>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.configure.wlanMeas.multiEval.listPy.segment.repcap_segmentB_get()
	driver.configure.wlanMeas.multiEval.listPy.segment.repcap_segmentB_set(repcap.SegmentB.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_ListPy_Segment_Bandwidth.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Btype.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_EnvelopePower.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Frequency.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Moffset.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Mtime.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Result.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Rtrigger.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Scount.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Setup.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_SingleCmw.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Standard.rst
	Configure_WlanMeas_MultiEval_ListPy_Segment_Stime.rst