EvmData
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.EvmData.EvmDataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.listPy.modulation.evmData.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_ListPy_Modulation_EvmData_Average.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmData_Current.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmData_Maximum.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmData_Minimum.rst
	WlanMeas_MultiEval_ListPy_Modulation_EvmData_StandardDev.rst