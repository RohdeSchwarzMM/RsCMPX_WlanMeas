Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:AVERage
	single: READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:AVERage

.. code-block:: python

	CALCulate:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:AVERage
	READ:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:PVTime:REDGe:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.PowerVsTime.RisingEdge.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: