FecCoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:FECCoding

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:VHTSig:FECCoding



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.VhtSig.FecCoding.FecCodingCls
	:members:
	:undoc-members:
	:noindex: