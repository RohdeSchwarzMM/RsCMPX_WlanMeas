Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: