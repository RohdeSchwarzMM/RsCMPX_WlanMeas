Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:ACSiso:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Acsiso.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: