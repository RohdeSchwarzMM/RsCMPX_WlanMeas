Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:SYMBol:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Symbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: