Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MIMO<n>:SEGMent<seg>:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MIMO<n>:SEGMent<seg>:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MIMO<n>:SEGMent<seg>:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:PVTime:MIMO<n>:SEGMent<seg>:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.PowerVsTime.Mimo.Segment.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: