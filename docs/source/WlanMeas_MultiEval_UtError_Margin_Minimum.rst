Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MINimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MINimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MINimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MINimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MINimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:MARGin:MINimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Margin.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: