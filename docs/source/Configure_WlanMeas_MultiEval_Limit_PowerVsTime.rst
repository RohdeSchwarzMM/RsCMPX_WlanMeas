PowerVsTime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:REDGe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:FEDGe
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:TERRor
	single: CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:TEDistrib

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:REDGe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:FEDGe
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:TERRor
	CONFigure:WLAN:MEASurement<Instance>:MEValuation:LIMit:PVTime:TEDistrib



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex: