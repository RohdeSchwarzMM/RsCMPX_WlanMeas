Crc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:CRC

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:SINFo:HTSig:CRC



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Htsig.Crc.CrcCls
	:members:
	:undoc-members:
	:noindex: