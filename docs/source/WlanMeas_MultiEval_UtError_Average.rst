Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:UTERror<n>:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.UtError.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: