Antenna<Antenna>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.tmode.antenna.repcap_antenna_get()
	driver.wlanMeas.tmode.antenna.repcap_antenna_set(repcap.Antenna.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:TMODe:ANTenna<Antennas>
	single: FETCh:WLAN:MEASurement<Instance>:TMODe:ANTenna<Antennas>
	single: INITiate:WLAN:MEASurement<Instance>:TMODe:ANTenna<Antennas>

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:TMODe:ANTenna<Antennas>
	FETCh:WLAN:MEASurement<Instance>:TMODe:ANTenna<Antennas>
	INITiate:WLAN:MEASurement<Instance>:TMODe:ANTenna<Antennas>



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.Tmode.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.tmode.antenna.clone()