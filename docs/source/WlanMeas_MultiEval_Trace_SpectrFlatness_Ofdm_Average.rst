Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness[:OFDM]:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Ofdm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: