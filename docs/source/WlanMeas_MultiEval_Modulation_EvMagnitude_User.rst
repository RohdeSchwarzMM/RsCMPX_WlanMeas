User<User>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr144
	rc = driver.wlanMeas.multiEval.modulation.evMagnitude.user.repcap_user_get()
	driver.wlanMeas.multiEval.modulation.evMagnitude.user.repcap_user_set(repcap.User.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.EvMagnitude.User.UserCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.evMagnitude.user.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Average.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Current.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Maximum.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User_StandardDev.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Stream.rst