StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:EVMall:SDEViation

.. code-block:: python

	FETCh:WLAN:MEASurement<Instance>:MEValuation:LIST:MODulation:EVMall:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.ListPy.Modulation.EvmAll.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: