Stream<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.modulation.evMagnitude.user.stream.repcap_stream_get()
	driver.wlanMeas.multiEval.modulation.evMagnitude.user.stream.repcap_stream_set(repcap.Stream.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.EvMagnitude.User.Stream.StreamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.modulation.evMagnitude.user.stream.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Stream_Average.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Stream_Current.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Stream_Maximum.rst
	WlanMeas_MultiEval_Modulation_EvMagnitude_User_Stream_StandardDev.rst