Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:AVERage
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:AVERage
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Frequency.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: