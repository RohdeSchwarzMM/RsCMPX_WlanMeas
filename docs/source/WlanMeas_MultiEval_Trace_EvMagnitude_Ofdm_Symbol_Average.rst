Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:OFDM:SYMBol:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Ofdm.Symbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: