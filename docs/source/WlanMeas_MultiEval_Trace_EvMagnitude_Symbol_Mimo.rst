Mimo<Mimo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.trace.evMagnitude.symbol.mimo.repcap_mimo_get()
	driver.wlanMeas.multiEval.trace.evMagnitude.symbol.mimo.repcap_mimo_set(repcap.Mimo.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Symbol.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.trace.evMagnitude.symbol.mimo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Trace_EvMagnitude_Symbol_Mimo_Average.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Symbol_Mimo_Current.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Symbol_Mimo_Maximum.rst
	WlanMeas_MultiEval_Trace_EvMagnitude_Symbol_Mimo_Minimum.rst