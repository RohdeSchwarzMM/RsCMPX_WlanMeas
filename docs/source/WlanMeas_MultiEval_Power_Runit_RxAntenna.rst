RxAntenna<RxAntenna>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.wlanMeas.multiEval.power.runit.rxAntenna.repcap_rxAntenna_get()
	driver.wlanMeas.multiEval.power.runit.rxAntenna.repcap_rxAntenna_set(repcap.RxAntenna.Nr1)





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.Runit.RxAntenna.RxAntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.power.runit.rxAntenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Power_Runit_RxAntenna_Average.rst
	WlanMeas_MultiEval_Power_Runit_RxAntenna_Current.rst
	WlanMeas_MultiEval_Power_Runit_RxAntenna_Maximum.rst
	WlanMeas_MultiEval_Power_Runit_RxAntenna_StandardDev.rst