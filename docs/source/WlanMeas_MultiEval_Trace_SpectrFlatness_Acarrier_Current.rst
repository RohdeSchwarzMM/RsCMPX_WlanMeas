Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:CURRent
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:CURRent
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:CURRent

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:CURRent
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:CURRent
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TRACe:SFLatness:ACARrier:CURRent



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.SpectrFlatness.Acarrier.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: