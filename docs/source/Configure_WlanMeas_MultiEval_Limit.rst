Limit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:UTEPower
	single: CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:UTERror

.. code-block:: python

	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:UTEPower
	CONFigure:WLAN:MEASurement<instance>:MEValuation:LIMit:UTERror



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.MultiEval.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.multiEval.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_WlanMeas_MultiEval_Limit_Modulation.rst
	Configure_WlanMeas_MultiEval_Limit_PowerVsTime.rst
	Configure_WlanMeas_MultiEval_Limit_SpectrFlatness.rst
	Configure_WlanMeas_MultiEval_Limit_TsMask.rst