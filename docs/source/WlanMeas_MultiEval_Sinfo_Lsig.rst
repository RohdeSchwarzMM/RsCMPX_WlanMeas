Lsig
----------------------------------------





.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Sinfo.Lsig.LsigCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.wlanMeas.multiEval.sinfo.lsig.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	WlanMeas_MultiEval_Sinfo_Lsig_Length.rst
	WlanMeas_MultiEval_Sinfo_Lsig_Parity.rst
	WlanMeas_MultiEval_Sinfo_Lsig_Rate.rst
	WlanMeas_MultiEval_Sinfo_Lsig_Reserved.rst
	WlanMeas_MultiEval_Sinfo_Lsig_Tail.rst