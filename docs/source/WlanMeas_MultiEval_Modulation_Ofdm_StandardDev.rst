StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:SDEViation
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:SDEViation
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:SDEViation

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:SDEViation
	FETCh:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:SDEViation
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:MODulation:OFDM:SDEViation



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Modulation.Ofdm.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: