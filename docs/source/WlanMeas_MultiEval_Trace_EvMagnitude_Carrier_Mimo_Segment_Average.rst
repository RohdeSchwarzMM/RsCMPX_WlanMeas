Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CARRier:MIMO<n>:SEGMent<seg>:AVERage
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CARRier:MIMO<n>:SEGMent<seg>:AVERage

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CARRier:MIMO<n>:SEGMent<seg>:AVERage
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CARRier:MIMO<n>:SEGMent<seg>:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Trace.EvMagnitude.Carrier.Mimo.Segment.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: