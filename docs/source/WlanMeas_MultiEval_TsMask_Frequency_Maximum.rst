Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:FREQuency:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Frequency.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: