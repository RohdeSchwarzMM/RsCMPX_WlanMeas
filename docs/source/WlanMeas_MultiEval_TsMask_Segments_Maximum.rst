Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MAXimum
	single: FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MAXimum
	single: CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MAXimum

.. code-block:: python

	READ:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MAXimum
	FETCh:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MAXimum
	CALCulate:WLAN:MEASurement<Instance>:MEValuation:TSMask:SEGMents:MAXimum



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.TsMask.Segments.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: