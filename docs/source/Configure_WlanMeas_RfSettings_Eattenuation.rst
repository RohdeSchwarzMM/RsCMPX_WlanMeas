Eattenuation<Connector>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.wlanMeas.rfSettings.eattenuation.repcap_connector_get()
	driver.configure.wlanMeas.rfSettings.eattenuation.repcap_connector_set(repcap.Connector.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:MEASurement<Instance>:RFSettings:EATTenuation<antenna>

.. code-block:: python

	CONFigure:WLAN:MEASurement<Instance>:RFSettings:EATTenuation<antenna>



.. autoclass:: RsCMPX_WlanMeas.Implementations.Configure.WlanMeas.RfSettings.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.wlanMeas.rfSettings.eattenuation.clone()