Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RUNit<ru>:AVERage

.. code-block:: python

	FETCh:WLAN:MEASurement<instance>:MEValuation:POWer:RUNit<ru>:AVERage



.. autoclass:: RsCMPX_WlanMeas.Implementations.WlanMeas.MultiEval.Power.Runit.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: