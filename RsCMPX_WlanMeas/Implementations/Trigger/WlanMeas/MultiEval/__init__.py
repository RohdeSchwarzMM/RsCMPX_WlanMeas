from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from .....Internal.Utilities import trim_str_response
from ..... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class MultiEvalCls:
	"""MultiEval commands group definition. 7 total commands, 1 Subgroups, 6 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("multiEval", core, parent)

	@property
	def catalog(self):
		"""catalog commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_catalog'):
			from .Catalog import CatalogCls
			self._catalog = CatalogCls(self._core, self._cmd_group)
		return self._catalog

	def get_source(self) -> str:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:SOURce \n
		Snippet: value: str = driver.trigger.wlanMeas.multiEval.get_source() \n
		Selects the source of the trigger events. Some values are always available. They are listed below. Depending on the
		installed options, additional values are available. You can query a list of all supported values via TRIGger:...
		:CATalog:SOURce?. \n
			:return: trig_source: 'IF Power': Power trigger (received RF power)
		"""
		response = self._core.io.query_str('TRIGger:WLAN:MEASurement<Instance>:MEValuation:SOURce?')
		return trim_str_response(response)

	def set_source(self, trig_source: str) -> None:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:SOURce \n
		Snippet: driver.trigger.wlanMeas.multiEval.set_source(trig_source = 'abc') \n
		Selects the source of the trigger events. Some values are always available. They are listed below. Depending on the
		installed options, additional values are available. You can query a list of all supported values via TRIGger:...
		:CATalog:SOURce?. \n
			:param trig_source: 'IF Power': Power trigger (received RF power)
		"""
		param = Conversions.value_to_quoted_str(trig_source)
		self._core.io.write(f'TRIGger:WLAN:MEASurement<Instance>:MEValuation:SOURce {param}')

	def get_mgap(self) -> float:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:MGAP \n
		Snippet: value: float = driver.trigger.wlanMeas.multiEval.get_mgap() \n
		Sets a minimum time during which the IF signal must be below the trigger threshold before the trigger is armed so that an
		IF power trigger event can be generated. \n
			:return: min_trig_gap: No help available
		"""
		response = self._core.io.query_str('TRIGger:WLAN:MEASurement<Instance>:MEValuation:MGAP?')
		return Conversions.str_to_float(response)

	def set_mgap(self, min_trig_gap: float) -> None:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:MGAP \n
		Snippet: driver.trigger.wlanMeas.multiEval.set_mgap(min_trig_gap = 1.0) \n
		Sets a minimum time during which the IF signal must be below the trigger threshold before the trigger is armed so that an
		IF power trigger event can be generated. \n
			:param min_trig_gap: No help available
		"""
		param = Conversions.decimal_value_to_str(min_trig_gap)
		self._core.io.write(f'TRIGger:WLAN:MEASurement<Instance>:MEValuation:MGAP {param}')

	def get_threshold(self) -> float or bool:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:THReshold \n
		Snippet: value: float or bool = driver.trigger.wlanMeas.multiEval.get_threshold() \n
		Defines the trigger threshold for power trigger sources. \n
			:return: trig_threshold: (float or boolean) No help available
		"""
		response = self._core.io.query_str('TRIGger:WLAN:MEASurement<Instance>:MEValuation:THReshold?')
		return Conversions.str_to_float_or_bool(response)

	def set_threshold(self, trig_threshold: float or bool) -> None:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:THReshold \n
		Snippet: driver.trigger.wlanMeas.multiEval.set_threshold(trig_threshold = 1.0) \n
		Defines the trigger threshold for power trigger sources. \n
			:param trig_threshold: (float or boolean) No help available
		"""
		param = Conversions.decimal_or_bool_value_to_str(trig_threshold)
		self._core.io.write(f'TRIGger:WLAN:MEASurement<Instance>:MEValuation:THReshold {param}')

	# noinspection PyTypeChecker
	def get_slope(self) -> enums.TriggerSlope:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:SLOPe \n
		Snippet: value: enums.TriggerSlope = driver.trigger.wlanMeas.multiEval.get_slope() \n
		Qualifies whether the trigger event is generated at the rising or at the falling edge of the trigger pulse (valid for
		external and power trigger sources) . \n
			:return: trig_slope: REDGe: Rising edge FEDGe: Falling edge
		"""
		response = self._core.io.query_str('TRIGger:WLAN:MEASurement<Instance>:MEValuation:SLOPe?')
		return Conversions.str_to_scalar_enum(response, enums.TriggerSlope)

	def set_slope(self, trig_slope: enums.TriggerSlope) -> None:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:SLOPe \n
		Snippet: driver.trigger.wlanMeas.multiEval.set_slope(trig_slope = enums.TriggerSlope.FEDGe) \n
		Qualifies whether the trigger event is generated at the rising or at the falling edge of the trigger pulse (valid for
		external and power trigger sources) . \n
			:param trig_slope: REDGe: Rising edge FEDGe: Falling edge
		"""
		param = Conversions.enum_scalar_to_str(trig_slope, enums.TriggerSlope)
		self._core.io.write(f'TRIGger:WLAN:MEASurement<Instance>:MEValuation:SLOPe {param}')

	def get_timeout(self) -> float or bool:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:TOUT \n
		Snippet: value: float or bool = driver.trigger.wlanMeas.multiEval.get_timeout() \n
		Selects the maximum time that the measurement waits for a trigger event before it stops in remote control mode or
		indicates a trigger timeout in manual operation mode. \n
			:return: trig_time_out: (float or boolean) No help available
		"""
		response = self._core.io.query_str('TRIGger:WLAN:MEASurement<Instance>:MEValuation:TOUT?')
		return Conversions.str_to_float_or_bool(response)

	def set_timeout(self, trig_time_out: float or bool) -> None:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:TOUT \n
		Snippet: driver.trigger.wlanMeas.multiEval.set_timeout(trig_time_out = 1.0) \n
		Selects the maximum time that the measurement waits for a trigger event before it stops in remote control mode or
		indicates a trigger timeout in manual operation mode. \n
			:param trig_time_out: (float or boolean) No help available
		"""
		param = Conversions.decimal_or_bool_value_to_str(trig_time_out)
		self._core.io.write(f'TRIGger:WLAN:MEASurement<Instance>:MEValuation:TOUT {param}')

	def get_offset(self) -> float:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:OFFSet \n
		Snippet: value: float = driver.trigger.wlanMeas.multiEval.get_offset() \n
		Sets the offset to the trigger event. The offset is useful if the trigger event and the burst are not synchronous.
		Triggering a measurement at another time can yield a synchronization error. \n
			:return: trig_offset: No help available
		"""
		response = self._core.io.query_str('TRIGger:WLAN:MEASurement<Instance>:MEValuation:OFFSet?')
		return Conversions.str_to_float(response)

	def set_offset(self, trig_offset: float) -> None:
		"""SCPI: TRIGger:WLAN:MEASurement<Instance>:MEValuation:OFFSet \n
		Snippet: driver.trigger.wlanMeas.multiEval.set_offset(trig_offset = 1.0) \n
		Sets the offset to the trigger event. The offset is useful if the trigger event and the burst are not synchronous.
		Triggering a measurement at another time can yield a synchronization error. \n
			:param trig_offset: No help available
		"""
		param = Conversions.decimal_value_to_str(trig_offset)
		self._core.io.write(f'TRIGger:WLAN:MEASurement<Instance>:MEValuation:OFFSet {param}')

	def clone(self) -> 'MultiEvalCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = MultiEvalCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
